===================================================
Initial Configuration file for Chikyu Kuru Kuru
	Configuration line starts with single "#", e.g., #Latitude, #Spin.
===================================================
----- Initial view point ------
#Latitude 0.0  :[-90,90]
#Longitude 135.0 :[-180,180]
#Perspective 0:[0: orthogonal, 1: perspective]
#EyePosition 80.0

===================================================
----- Initial setup for animation & spin ------
#Animation 1 :[0: stop, 1: play, -1: reverse play]
#AnimationSpeed 8 :[-30,30]
#StopAt1stMap 1.0
#StopAtFinalMap 1.0
#Repeat 1 

#Spin 1
#SpinSpeed 20 :[-200,200]

===================================================
----- Initial setup for widow size & fullscreen (mac only) ------
#WindowSizeXY 1024 768
#FullScreen 0

===================================================
----- Image file names ------
--- Window 1 ---
#TextureName data/images/map/map_
#ScreenName data/images/screen/screen_
#SecondTextureName data/images/map_second/map_
#SecondScreenName data/images/screen_second/screen_

#TextureSuffix jpg
#SecondTextureSuffix jpg
#ScreenSuffix jpg
#SecondScreenSuffix jpg    

--- Window 2 ---
!#TextureName2 data/images_2/map/map_
#ScreenName2 data/images_2/screen/screen_
#TextureName2 data/images/map/map_
!#ScreenName2 data/images/screen/screen_

#SecondTextureName2 data/images_2/map_second/map_
#SecondScreenName2 data/images_2/screen_second/screen_

#TextureSuffix2 jpg
#SecondTextureSuffix2 jpg
#ScreenSuffix2 jpg
#SecondScreenSuffix2 jpg    
----------------------	


#TextureStart 0
#TextureEnd 9999
#SecondTexture_at_startup 0

===================================================
----- GUI ------
#InvisibleCursor 0
#ClickLock 0
#NoMouseDragRotation 0


!#Icon 0                                          ### For no Icons
!#Icon 6
!#Icon 13                                        ### Without Fullscreen
!#Icon 14                                       ### No screen control, layers & icon hide
#Icon 27                                      ### Full Icons

#IconScaleXY 0.2 2.4 -1.5		       ###Play/Stop
#Icon2ScaleXY 0.2 1.9 -1.75	        ###Initial
#Icon3ScaleXY 0.2 2.15 -1.5		     ###Reverse Play/Stop
#Icon4ScaleXY 0.2 2.4 1.3		       ###Spin/Stop
#Icon5ScaleXY 0.2 2.4 -1.75		      ###1step Forward
#Icon27ScaleXY 0.2 1.9   -1.5		  ###1step Forward & Change Layer
#Icon6ScaleXY 0.2 2.15 -1.75		 ###1step Backward
#Icon7ScaleXY 0.2 2.4 1.05		       ###Latitude/Longitude Lines
#Icon8ScaleXY 0.2 2.4 1.55		       ###Zoom in
#Icon9ScaleXY 0.2 2.15 1.55		      ###Zoom out
#Icon10ScaleXY 0.2 2.15 1.3		      ###North upward
#Icon11ScaleXY 0.2 2.4 -1.25	      ### Fast (Play speed)
#Icon12ScaleXY 0.2 2.15 -1.25	      ### Slow (Play speed)
#Icon13ScaleXY 0.2 2.4 1.95		        ###Quit
#Icon14ScaleXY 0.2 2.15 1.95		  ###Full Screen

#Icon15ScaleXY 0.18 -2.20 1.95		  ###Logo Small
#Icon16ScaleXY 0.18 -1.995 1.95		  ###Logo Large
#Icon17ScaleXY 0.2 -2.65 1.85		  ###Logo Left
#Icon18ScaleXY 0.2 -2.45 1.85		  ###Logo Right
#Icon19ScaleXY 0.2 -2.55  1.65		  ###Logo Down
#Icon20ScaleXY 0.2 -2.55  2.05		  ###Logo Up

#Icon21ScaleXY 0.2 -0.85  -1.75		  ###Switch Layers
#Icon22ScaleXY 0.2 -1.25  -1.75		  ###Screen Show/Hide
#Icon23ScaleXY 0.2 -1.5  -1.75		  ###Icon Show/Hide

#Icon24ScaleXY 0.2 2.4  -0.5	        ###Icon Drawing mode / Rotation mode 
#Icon25ScaleXY 0.2 2.4   -0.75		  ###Icon Erase drawings
#Icon26ScaleXY 0.2 2.4   -1.		   ###Icon Save drawings
#PaletteScaleXY 0.1 2.6 0.51           ###Color palette for drawing: scale, x, y 
#LineThicknessColor 2.5 1. 0.373 0.125 1.0 # Drawing line thickness, R, G, B, alpha

--- If don't like to show following icons ---
#IconScaleXY 0 2.4 -1.5		       ###Play/Stop
#Icon2ScaleXY 0. 1.9 -1.75	        ###Initial
#Icon3ScaleXY 0. 2.15 -1.5		     ###Reverse Play/Stop
#Icon4ScaleXY 0. 2.4 1.3		       ###Spin/Stop
#Icon5ScaleXY 0. 2.4 -1.75		      ###1step Forward
#Icon27ScaleXY 0. 2.4   -0.25		  ###1step Forward & Change Layer
#Icon6ScaleXY 0. 2.15 -1.75		 ###1step Backward
#Icon7ScaleXY 0. 2.4 1.05		       ###Latitude/Longitude Lines
#Icon8ScaleXY 0. 2.4 1.55		       ###Zoom in
#Icon9ScaleXY 0. 2.15 1.55		      ###Zoom out
#Icon10ScaleXY 0. 2.15 1.3		      ###North upward
#Icon11ScaleXY 0. 2.4 -1.25	      ### Fast (Play speed)
#Icon12ScaleXY 0. 2.15 -1.25	      ### Slow (Play speed)
#Icon13ScaleXY 0. 2.4 1.95		        ###Quit
#Icon14ScaleXY 0. 2.15 1.95		  ###Full Screen

#Icon15ScaleXY 0. -2.20 1.95		  ###Logo Small
#Icon16ScaleXY 0. -1.995 1.95		  ###Logo Large
#Icon17ScaleXY 0. -2.65 1.85		  ###Logo Left
#Icon18ScaleXY 0. -2.45 1.85		  ###Logo Right
#Icon19ScaleXY 0. -2.55  1.65		  ###Logo Down
#Icon20ScaleXY 0. -2.55  2.05		  ###Logo Up

#Icon21ScaleXY 0. -0.85  -1.75		  ###Switch Layers
#Icon22ScaleXY 0. -1.25  -1.75		  ###Screen Show/Hide
#Icon23ScaleXY 0. -1.5  -1.75		  ###Icon Show/Hide

#Icon24ScaleXY 0. 2.4  -0.5	        ###Icon Drawing mode / Rotation mode 
#Icon25ScaleXY 0. 2.4   -0.75		  ###Icon Erase drawings
#Icon26ScaleXY 0. 2.4   -1.		   ###Icon Save drawings
===================================================
----- Size and Locations of Earth, Screen (caption) images and Timebar ------

--- Earth Right, Caption Left ----
#NumberOfEarth 1
#Scale 1.9
#EarthXY 0.3 0.0
#ScreenScaleXY 4.3 -2.6 1.9
#ScreenFront 0 :[0:  Caption is behind the Earth. 1: Caption is in front of the Earth.]
#TimebarScaleXY .2 -1.6 -1.65

--- Earth Left, Caption Right and small----
!#NumberOfEarth 1
!#Scale 1.9
!#EarthXY -0.35 0.0
!#ScreenScaleXY 3.2 1.28 1.5 
!#ScreenFront 0  :[0:  Caption is behind the Earth. 1: Caption is in front of the Earth.]
!#TimebarScaleXY .18 1.5 -1.65

--- Earth Left, Caption on the Earth----
!#NumberOfEarth 1
!#Scale 1.9
!#EarthXY -0.35 0.0
!#ScreenScaleXY 3. -1. 1.
!#ScreenFront 1  :[0:  Caption is behind the Earth. 1: Caption is in front of the Earth.]
!#TimebarScaleXY .2 -2.5 -1.65

--- Earth Center, Caption on the Earth ----
!#NumberOfEarth 1
!#Scale 1.9
!#EarthXY 0.0 0.0
!#ScreenScaleXY 4.3 -0.6 1.9
!#ScreenFront 1  :[0:  Caption is behind the Earth. 1: Caption is in front of the Earth.]
!#TimebarScaleXY .2 -2.5 -1.65

--- No Earth, Caption Only ----
!#NumberOfEarth 1
!#Scale 0.
!#EarthXY 0.3 0.0
!#NumberOfEarth 1
!#ScreenScaleXY 3.94 -3.12 1.97
!#ScreenFront 0  :[0:  Caption is behind the Earth. 1: Caption is in front of the Earth.]
!#TimebarScaleXY .17 -3.1 -1.75

-------- 2 Windows ---------
--- for 2 Earth - 3D  --
!#NumberOfEarth 2
!#TextureOfEarth2 0
!#EarthRotationSynch 1  ### Rotation Sync
!#EarthSizeSynch 1    ###  Size sync
!#Earth2AngleAxisXY -10 0.0 1.0 ### Angle between two Earths.

--- for 2 Earth - Dual Projector  --
!#NumberOfEarth 2
!#TextureOfEarth2 0
!#EarthRotationSynch 1  ### Rotation Sync
!#EarthSizeSynch 1    ### Size sync
!#Earth2AngleAxisXY -150 0.0 1.0 ### Angle between two Earths.

--- for 2 Earth Normal & Zoom In---
!#NumberOfEarth 2
!#TextureOfEarth2 0
!#EarthRotationSynch 1  ### Rotation Sync
!#EarthSizeSynch 0    ### Size sync
!#Scale2 6.0 ### Scale if size is not sync.
!#Earth2AngleAxisXY 0.0 0.0 1.0  ### Angle between two Earths.

--- for 2 Earth & 2 Textures Sync for 2 parameters ---
!#NumberOfEarth 2
!#TextureOfEarth2 1
!#ScreenScaleXY2 4.3 -2.6 1.9
!#EarthRotationSynch 1  ### Rotation Sync
!#EarthSizeSynch 1    ### Size sync
!#Earth2AngleAxisXY 0.0 0.0 1.0 ### Angle between two Earths.

--- for 2 Earth & 2 Textures No Sync (only Time Sync) for 2 planets/moons  ---
!#NumberOfEarth 2
!#TextureOfEarth2 1
!#EarthRotationSynch 0  ### Rotation Sync
!#Latitude2 0.0  :[-90,90] ### Initial coordinate if rotation is not sync.
!#Longitude2 0.0 :[-180,180] ### Initial coordinate if rotation is not sync.
!#EarthSizeSynch 0    ### Size sync
!#Scale2 1.9 	### Scale if size is not sync.

--- for Earth window and Operation window ---
#Animation 0
#NumberOfEarth 2
#TextureOfEarth2 1
#EarthRotationSynch 1  ### Rotation Sync
#EarthSizeSynch 0    ### Size sync
#Earth2AngleAxisXY 0.0 0.0 1.0 ### Angle between two Earths.
#Scale 1.9
#Scale2 1.9 ### Scale if size is not sync.
#EarthXY 0.0 0.0
#ScreenFront 1  :[0:  Caption is behind the Earth. 1: Caption is in front of the Earth.]
#ScreenScaleXY 4 -2.5 2.
#ScreenScaleXY2 6 -0.9 0.9
#TextureName data/images/dum/dum_
#TextureName2 data/images/map/map_
#TimebarScaleXY 0. -3.1 -1.75

----- 2 choice Quiz -----
#Icon 27
#Icon5ScaleXY 1.2 1.5 0.7		      ###1step Forward
#Icon27ScaleXY 1.2 1.5 -0.5		      ###1step Forward


===================================================
----- GRID: Latitude & Longitude lines ------
#MeridianLatitude 0 12 6 3. 5.
#ColorMeridianLatitude 0.7 0.7 0.7 0.7  0.8 0.2 0.2 0.7

===================================================
----- Misc ------
#SpinIntervalSecond 0.01
#InertiaOfRotation 0.02 0.01 0.01 0. : [Start_criteria, Stop_criteria, Speed of rotation, Damping rate (1= little damping, 0 = no inertia rotation)]
===================================================
