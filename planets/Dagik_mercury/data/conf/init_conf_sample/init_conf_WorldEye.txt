===================================================
Initial Configuration file for Chikyu Kuru Kuru
	Configuration line starts with single "#", e.g., #Latitude, #Spin.
===================================================

----- Initial setup for widow size & fullscreen ------
#WindowSizeXY 800 600
#WindowPositionXY 0 0
#FullScreen 0

#WindowSizeXY2  1024 768
#WindowPositionXY2 1024 0
#FullScreen2 0

===================================================

----- Misc. ------
#InvisibleCursor 0
#ClickLock 0
#NoMouseDragRotation 0
#PresentaionRemoteMode 0 : [ For Presentation Remote Controller: '->'key = a, '<-'key = b ]
#InertiaOfRotation 0.02 0.01 0.01 0. : [Start_criteria, Stop_criteria, Speed of rotation, Damping rate (1= little damping, 0 = no inertia rotation)]

===================================================

-------- Earth Size & Position --------
-------- Backward projection ---------
#Perspective 1 :[0: orthogonal, 1: perspective]
..... 110 degree (for World Eye by Gakken Sta:ful) .....
#EyePosition -4.
#Scale 2.04

... For Backward projection, the size & the location of the Earth of two windows must be same. ...
... This could be a bug of ckk. ...
... Window resolution of World Eye should be 1024x768 ...

#NumberOfEarth 2
#TextureOfEarth2 1
#EarthSizeSynch 1
#EarthRotationSynch 1  ### Rotation Sync

#EarthXY  0.0 0.0

#ScreenScaleXY 4.3 -1.0 1.9   
#ScreenScaleXY2 3. -0.5 1.    : [Caption is center.]
#ScreenFront 1  :[0:  Caption is behind the Earth. 1: Caption is in front of the Earth.]
#TimebarScaleXY .15 -2.5 -1.65

... Not to display the Earth on the control window (1st window)
#TextureName data/images/map/map_dum
#SecondTextureName data/images/map_second/map_dum
===================================================

----- Initial Latitude & Longitude ------
#Latitude 0.0  :[-90,90]
#Longitude 135.0 :[-180,180]

===================================================

----- Initial setup for animation & spin ------
#Animation 1 :[0: stop, 1: play, -1: reverse play]
#AnimationSpeed 8 :[-30,30]
#StopAt1stMap 1.0
#StopAtFinalMap 1.0
#Repeat 1 

#Spin 0
#SpinSpeed 10 :[-200,200]
#SpinIntervalSecond 0.005 :[Smaller number gets faster spin.]

===================================================

----- View point ------
!#Perspective 0:[0: orthogonal, 1: perspective]
!#EyePosition 80.0

===================================================
#NumberOfTextures 1  :[ 1 or 2 ]
#SecondTexture_at_startup 0

----- Image file names ------
--- Window 1 ---
!#TextureName data/images/map/map_
#ScreenName data/images/screen/screen_
!#SecondTextureName data/images/map_second/map_
#SecondScreenName data/images/screen_second/screen_

#TextureSuffix jpg
#SecondTextureSuffix jpg
#ScreenSuffix jpg
#SecondScreenSuffix jpg    

--- Window 2 ---
#TextureName2 data/images/map/map_
#ScreenName2 data/images/screen/screen_
#SecondTextureName2 data/images/map_second/map_
#SecondScreenName2 data/images/screen_second/screen_

#TextureSuffix2 jpg
#SecondTextureSuffix2 jpg
#ScreenSuffix2 jpg
#SecondScreenSuffix2 jpg    
----------------------	

#TextureStart 0
#TextureEnd 9999
#SecondTexture_at_startup 0

===================================================
-------- 1 Window --------
--- Earth Right, Caption Left ----
!#NumberOfEarth 1
!#Scale 1.9
!#EarthXY 0.3 0.0
!#ScreenScaleXY 4.3 -2.6 1.9
!#ScreenFront 0 :[0:  Caption is behind the Earth. 1: Caption is in front of the Earth.]
!#TimebarScaleXY .2 -1.6 -1.65

--- Earth Center, Caption on the Earth ----
!#NumberOfEarth 1
!#Scale 1.9
!#EarthXY 0.0 0.0

!#ScreenScaleXY 3. -0.5 1.    : [Caption is center.]
!!#ScreenScaleXY 6 -1.3 -1.    : [Caption is in lower part.]
!#ScreenFront 1  :[0:  Caption is behind the Earth. 1: Caption is in front of the Earth.]
!#TimebarScaleXY .2 -2.65 -1.65

--- No Earth, Caption Only ----
!#NumberOfEarth 1
!#Scale 0.
!#EarthXY 0.3 0.0
!#NumberOfEarth 1
!#ScreenScaleXY 3.94 -3.12 1.97
!#ScreenFront 0  :[0:  Caption is behind the Earth. 1: Caption is in front of the Earth.]
!#TimebarScaleXY .17 -3.1 -1.75

-------- Backward projection ---------
!#Perspective 1 :[0: orthogonal, 1: perspective]
!#EarthXY 0. 0.0
!#ScreenScaleXY 3. -0.5 1.    : [Caption is center.]
!!#ScreenScaleXY 6 -1.3 -1.    : [Caption is in lower part.]
!#ScreenFront 1  :[0:  Caption is behind the Earth. 1: Caption is in front of the Earth.]
!#TimebarScaleXY .15 -2.5 -1.65

..... 95 degree (Minimum for Backward projection) .....
!#EyePosition -9.
!#Scale 1.9
..... 100 degree .....
!#EyePosition -7.
!#Scale 1.9
..... 110 degree .....
!#EyePosition -4.
!#Scale 1.75
..... 130 degree .....
!#EyePosition -2.
!#Scale 1.41
..... 140 degree .....
!#EyePosition -1.5
!#Scale 1.195
..... 155 degree (Maximum for Backward projection).....
!#EyePosition -.75
!#Scale .702
..... 110 degree (for World Eye by Gakken Sta:ful) .....
!#EyePosition -4.
!#Scale 2.04
........ Cannot use "EarthSizeSynch 0" for 2 windows & backward projection
!#EarthSizeSynch 1    ### Size sync

===================================================
-------- 2 Windows ---------
--- for 2 Earth & 2 Textures for 2 parameters ---
!#NumberOfEarth 2
!#TextureOfEarth2 1
!#EarthRotationSynch 1  ### Rotation Sync
!#EarthSizeSynch 1    ### Size sync
!#Earth2AngleAxisXY 0.0 0.0 1.0 ### Angle between two Earths.
!#ScreenScaleXY2 4.3 -2.6 1.9
!#TextureName2 data/images_2/map/map_
!#ScreenName2 data/images_2/screen/screen_
!#SecondTextureName2 data/images_2/map_second/map_
!#SecondScreenName2 data/images_2/screen_second/screen_

--- for Earth window and Control window ---
!#NumberOfEarth 2
!#TextureOfEarth2 1
!#EarthSizeSynch 0
!#EarthRotationSynch 1  ### Rotation Sync
!#Scale 0.75
!#Scale2 1.9
!#EarthXY  -0.5 0.0
!#Earth2XY 0.0 0.0
!#ScreenScaleXY 4.3 -1.0 1.9   
!#ScreenScaleXY2 3. -0.5 1.    : [Caption is center.]
!#ScreenFront 1  :[0:  Caption is behind the Earth. 1: Caption is in front of the Earth.]
!#TimebarScaleXY .2 -2.65 -1.65

===================================================
----- GRID: Latitude & Longitude lines ------
#MeridianLatitude 0 12 6 3. 5.
#ColorMeridianLatitude 0.7 0.7 0.7 0.7  0.8 0.2 0.2 0.7

===================================================
----- Drawing Pallete & LIne ------
#PaletteScaleXY 0.1 2.6 0.51           ###Color palette for drawing: scale, x, y 
#LineThicknessColor 2.5 1. 0.373 0.125 1.0 # Drawing line thickness, R, G, B, alpha

===================================================
----- Icon ------
!#Icon 0                                          ### For no Icons
#Icon 27                                      ### Full Icons

#IconScaleXY 0.2 2.4 -1.5		       ###Play/Stop
#Icon2ScaleXY 0.2 1.9 -1.75	        ###Initial
#Icon3ScaleXY 0.2 2.15 -1.5		     ###Reverse Play/Stop
#Icon4ScaleXY 0.2 2.4 1.3		       ###Spin/Stop
#Icon5ScaleXY 0.2 2.4 -1.75		      ###1step Forward
#Icon27ScaleXY 0.2 1.9   -1.5		  ###1step Forward & Change Layer
#Icon6ScaleXY 0.2 2.15 -1.75		 ###1step Backward
#Icon7ScaleXY 0.2 2.4 1.05		       ###Latitude/Longitude Lines
#Icon8ScaleXY 0.2 2.4 1.55		       ###Zoom in
#Icon9ScaleXY 0.2 2.15 1.55		      ###Zoom out
#Icon10ScaleXY 0.2 2.15 1.3		      ###North upward
#Icon11ScaleXY 0.2 2.4 -1.25	      ### Fast (Play speed)
#Icon12ScaleXY 0.2 2.15 -1.25	      ### Slow (Play speed)
#Icon13ScaleXY 0.2 2.4 1.95		        ###Quit
#Icon14ScaleXY 0.2 2.15 1.95		  ###Full Screen

#Icon15ScaleXY 0.18 -2.20 1.95		  ###Logo Small
#Icon16ScaleXY 0.18 -1.995 1.95		  ###Logo Large
#Icon17ScaleXY 0.2 -2.65 1.85		  ###Logo Left
#Icon18ScaleXY 0.2 -2.45 1.85		  ###Logo Right
#Icon19ScaleXY 0.2 -2.55  1.65		  ###Logo Down
#Icon20ScaleXY 0.2 -2.55  2.05		  ###Logo Up

#Icon21ScaleXY 0.2 -0.85  -1.75		  ###Switch Layers
#Icon22ScaleXY 0.2 -1.25  -1.75		  ###Screen Show/Hide
#Icon23ScaleXY 0.2 -1.5  -1.75		  ###Icon Show/Hide

#Icon24ScaleXY 0.2 2.4  -0.5	        ###Icon Drawing mode / Rotation mode 
#Icon25ScaleXY 0.2 2.4   -0.75		  ###Icon Erase drawings
#Icon26ScaleXY 0.2 2.4   -1.		   ###Icon Save drawings

--- If don't like to show following icons ---
!#IconScaleXY 0. 0. 0. 		        ###Play/Stop
!#Icon2ScaleXY  0. 0. 0. 		   ###Initial
!#Icon3ScaleXY  0. 0. 0. 		   ###Reverse Play/Stop
!#Icon4ScaleXY  0. 0. 0. 		   ###Spin/Stop
!#Icon5ScaleXY  0. 0. 0. 		   ###1step Forward
#Icon27ScaleXY 0. 0. 0. 		  ###1step Forward & Change Layer
!#Icon6ScaleXY 0. 0. 0. 		   ###1step Backward
!#Icon7ScaleXY 0. 0. 0. 	       ###Latitude/Longitude Lines
!#Icon8ScaleXY 0. 0. 0. 		   ###Zoom in
!#Icon9ScaleXY 0. 0. 0. 		   ###Zoom out
!#Icon10ScaleXY 0. 0. 0. 		  ###North upward
!#Icon11ScaleXY 0. 0. 0. 	      ### Fast (Play speed)
!#Icon12ScaleXY 0. 0. 0. 	      ### Slow (Play speed)
!#Icon13ScaleXY 0. 0. 0. 		  ###Quit
#Icon14ScaleXY 0. 0. 0. 		  ###Full Screen

#Icon15ScaleXY 0. 0. 0. 		  ###Logo Small
#Icon16ScaleXY 0. 0. 0. 		  ###Logo Large
#Icon17ScaleXY 0. 0. 0. 		  ###Logo Left
#Icon18ScaleXY 0. 0. 0. 		  ###Logo Right
#Icon19ScaleXY 0. 0. 0. 		  ###Logo Down
#Icon20ScaleXY 0. 0. 0. 		  ###Logo Up

!#Icon21ScaleXY 0. 0. 0. 		  ###Switch Layers
!#Icon22ScaleXY 0. 0. 0. 		  ###Screen Show/Hide
!#Icon23ScaleXY 0. 0. 0. 		  ###Icon Show/Hide

!#Icon24ScaleXY 0. 0. 0. 	       ###Icon Drawing mode / Rotation mode 
!#Icon25ScaleXY 0. 0. 0. 		   ###Icon Erase drawings
!#Icon26ScaleXY 0. 0. 0. 		   ###Icon Save drawings

===================================================
----- Specific layout ------
 ---- For Icon on bottom ----
!#IconScaleXY 0.2 2.4 -1.25		       ###Play/Stop
!#Icon2ScaleXY 0.2 1.9 -1.75	        ###Initial
!#Icon3ScaleXY 0.2 2.15 -1.25		     ###Reverse Play/Stop
!#Icon4ScaleXY 0.2 1.65 -1.5		       ###Spin/Stop
!#Icon5ScaleXY 0.2 1.9 -1.25		      ###1step Forward
!#Icon6ScaleXY 0.2 1.65 -1.25		 ###1step Backward
!#Icon8ScaleXY 0. 2.4 1.55		       ###Zoom in
!#Icon9ScaleXY 0. 2.15 1.55		      ###Zoom out
!#Icon7ScaleXY 0.2 1.9 -1.5		       ###Latitude/Longitude Lines
!#Icon10ScaleXY 0.2 1.65 -1.75		      ###North upward
!#Icon11ScaleXY 0.2 2.4 -1.5	      ### Fast (Play speed)
!#Icon12ScaleXY 0.2 2.15 -1.5	      ### Slow (Play speed)
!#Icon13ScaleXY 0.2 2.4 -1.75		        ###Quit
!#Icon14ScaleXY 0. 2.15 -1.75		  ###Full Screen
!#Icon21ScaleXY 0.2 -1.87  -1.75		  ###Switch Layers
!#Icon22ScaleXY 0.2 -2.35  -1.75		  ###Screen Show/Hide
!#Icon23ScaleXY 0.2 -2.6  -1.75		  ###Icon Show/Hide

----- For 2 choice Quiz -----
!#Icon5ScaleXY 1.2 1.5 0.7		      ###1step Forward
!#Icon27ScaleXY 1.2 1.5 -0.5		      ###1step Forward


---- For Operation Window -----
!#IconScaleXY  0.7 -2.1 0.4			       ###Play/Stop
!#Icon2ScaleXY 0.7 -2.55 -0.35	        ###Initial
!#Icon3ScaleXY 0.7 -3. 0.4		     ###Reverse Play/Stop
!#Icon10ScaleXY 0.7 -2.55 1.15		      ###North upward

===================================================
