--- No Earth, Caption Only ----
#Scale 0.
#NumberOfEarth 1
#ScreenScaleXY 1.5 -0.5 0.75
#ScreenFront 0  :[0:  Caption is behind the Earth. 1: Caption is in front of the Earth.]
#TimebarScaleXY .2 -2.65 -1.65

#Icon4ScaleXY 0. 12.4 1.3		       ###Spin/Stop
#Icon7ScaleXY 0. 12.4 1.05		       ###Latitude/Longitude Lines
#Icon8ScaleXY 0. 12.4 1.55		       ###Zoom in
#Icon9ScaleXY 0. 12.15 1.55		      ###Zoom out
#Icon10ScaleXY 0. 12.15 1.3		      ###North upward
!#Icon14ScaleXY 0. 12.15 1.95		  ###Full Screen

#Icon15ScaleXY 0. -2.20 1.95		  ###Logo Small
#Icon16ScaleXY 0. -1.995 1.95		  ###Logo Large
#Icon17ScaleXY 0. -2.65 1.85		  ###Logo Left
#Icon18ScaleXY 0. -2.45 1.85		  ###Logo Right
#Icon19ScaleXY 0. -2.55  1.65		  ###Logo Down
#Icon20ScaleXY 0. -2.55  2.05		  ###Logo Up
#Icon22ScaleXY 0.2 -2.1  -1.75		  ###Screen Show/Hide
#Icon23ScaleXY 0.2 -2.6  -1.75		  ###Icon Show/Hide
