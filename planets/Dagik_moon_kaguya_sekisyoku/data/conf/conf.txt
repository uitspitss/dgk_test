#NumberOfTextures 2  :[ 1 or 2 ]
!#SecondTexture_at_startup 0

#Longitude -90.

#Animation 0 :[0: stop, 1: play, -1: reverse play]
#IconScaleXY 0. 2.4 -1.5		       ###Play/Stop
#Icon2ScaleXY 0.2 1.9 1.55		        ###Initial
#Icon3ScaleXY 0. 2.15 -1.5		     ###Reverse Play/Stop
#Icon5ScaleXY 0. 2.4 -1.75		      ###1step Forward
#Icon6ScaleXY 0. 2.15 -1.75		 ###1step Backward
#Icon11ScaleXY 0. 2.4 -1.25	      ### Fast (Play speed)
#Icon12ScaleXY 0. 2.15 -1.25	      ### Slow (Play speed)
#TimebarScaleXY 0. 1.5 -1.65