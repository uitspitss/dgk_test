#IconScaleXY  0.7 -1.4 -0.55		       ###Play/Stop
#Icon2ScaleXY 0.7 -3.0 -1.25	        ###Initial
#Icon3ScaleXY 0.7 -2.2 -0.55		     ###Reverse Play/Stop
#Icon4ScaleXY 0.7 -1.4 1.1	       ###Spin/Stop
#Icon5ScaleXY 0.7 -1.4 -1.25		      ###1step Forward
#Icon6ScaleXY 0.7 -2.2 -1.25		 ###1step Backward
#Icon7ScaleXY 0.7 -3.0 1.1		       ###Latitude/Longitude Lines
#Icon8ScaleXY 0.7 -1.4 1.85		       ###Zoom in
#Icon9ScaleXY 0.7 -2.2 1.85 	      ###Zoom out
#Icon10ScaleXY 0.7 -2.2 1.1	      ###North upward
#Icon11ScaleXY 0.7 -1.4 0.2	      ### Fast (Play speed)
#Icon12ScaleXY 0.7 -2.2 0.2	      ### Slow (Play speed)
#Icon13ScaleXY 0.7 -3.0 1.95		        ###Quit

!#Icon21ScaleXY 0.7 -3.0 0.2		  ###Switch Layers
#Icon22ScaleXY 0.7 -3.0 -0.55	  ###Screen Show/Hide