==========================================================================
init_conf.txt：使用する機器に依存する設定ファイル。多くのコンテンツに共通する設定をする
	conf.txt（コンテンツ毎の設定）の設定が優先される。
	
		以下のファイルをdata/conf/conf.txtとしてコピーして下さい。
==========================================================================
init_conf.txt：通常用設定。
init_conf_Center.txt：地球画像を中央に表示し、キャプション画像を地球の上に表示する。
init_conf_DVD_starter.txt：導入用DVD用。通常用設定と同じ。
init_conf_Two-display.txt：２つのウィンドウを使用。１画面目は操作画面、２画面目は表示画面。
init_conf_WorldEye.txt：裏側からの広範囲投影用。学研ステイフル社ワールドアイ用の設定。
