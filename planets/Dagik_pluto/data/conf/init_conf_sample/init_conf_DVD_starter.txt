===================================================
Initial Configuration file for Chikyu Kuru Kuru
	Configuration line starts with single "#", e.g., #Latitude, #Spin.
===================================================
　　以下、CKKの設定をします
　　「#」で始まる行はコマンドとして実行命令が書かれています。
　　それ以外の文字で始まる行はコメントでして、実行には影響しません。

　　「!# 」で始まる行は、使っていない設定です。「!」を消すと設定が有効になります。

　　設定ファイルの優先順位は lang-で始まるもの、conf.txt、init_conf.txtです。
　　同じ項目が設定されている場合は、優先順位の高いファイルでの設定が
　　使われます。
　　lang- は、言語ごとの設定、conf.txtはコンテンツごとの設定、
　　init_conf.txt は投影環境（ハードウェアなど）ごとの設定に主に使われます。

----- Initial setup for widow size & fullscreen ------
　　画面の解像度（大きさ）を設定します。横幅、縦幅の順です。

　　PCの画面サイズに合わせると、画面いっぱいに表示されます。
　　PCの画面のサイズはデスクトップのプロパティから確認できます。
#WindowSizeXY 1024 768

　　画面の位置（ウィンドウの左上角の座標）を設定します。X座標Y座標の順です。
#WindowPositionXY 0 0

　　起動時に全画面表示にする場合は 1 に 、しない場合は 0 にしてください。
　　Windowsでは全画面表示がうまくいかない場合が多いので注意してください。
#FullScreen 0

　　２ウィンドウ目の画面の解像度、位置、全画面表示の設定です。
　　２ウィンドウを使うには、後で出てくるNumberOfEarth 2 と指定します。
!#WindowSizeXY2  1024 768
!#WindowPositionXY2 1024 0
!#FullScreen2 0

===================================================
----- Misc. ------
　　カーソルを表示しない場合は 1 に 、する場合は 0 にしてください。
　　タッチディスプレイで操作する場合は、カーソルを消すとスッキリします。
#InvisibleCursor 0

　　クリックロック（クリックしなくてもマウスを動かせば地球が回転する）に
　　する場合は 1 に 、しない場合は 0 にしてください。
　　２つのウィンドウ使う場合（NumberOfEarth 2）は挙動がおかしくなるので
　　使わないほうが良いです。
#ClickLock 0

　　マウスでドラッグしても、地球が回転しないようにする場合は 1 に 、
　　しない場合は 0 にしてください。
#NoMouseDragRotation 0

　　プレゼンテーション・リモコン・モードにする場合は 1 に 、
　　しない場合は 0 にしてください。
　　プレゼンテーション・リモコン・モードでは、左右の矢印キーがコマ送りの進む、戻るになります。
#PresentaionRemoteMode 0 : [ For Presentation Remote Controller: '->'key = a, '<-'key = b ]

　　地球を回して離したあとも、そのまま勢いでぐるぐる回り続ける設定です。
　　最後の数字を 1. にすると、ぐるぐる回り続け、0. にすると離したらすぐ止まります。
　　0.99 とかにすると、だんだん止まっていきます。
#InertiaOfRotation 0.02 0.01 0.01 0. : [Start_criteria, Stop_criteria, Speed of rotation, Damping rate (1= little damping, 0 = no inertia rotation)]

===================================================

-------- Earth Size & Position --------
--- Earth Right, Caption Left ----
　　地球画像を右、キャプション画面を左（モニター1台に出力）

　　ウィンドウの数が１つの場合。
#NumberOfEarth 1

　　地球画像の大きさの指定です。
#Scale 1.9

　　地球画像の位置の指定です。X座標Y座標の順です。
#EarthXY 0.3 0.0

　　キャプション画像の大きさの指定です。
#ScreenScaleXY 4.3 -2.6 1.9

　　キャプション画像と地球画像が重なった場合に、どちらを上にするかです。
　　0 の場合は地球画像が上、 1 の場合はキャプション画像が上になります。
#ScreenFront 0 :[0:  Caption is behind the Earth. 1: Caption is in front of the Earth.]

　　複数枚の画像がある場合に何枚目の画像を表示しているかを示す「バー」の
　　大きさと位置を指定しています。
#TimebarScaleXY .2 -1.6 -1.65

===================================================

----- Initial Latitude & Longitude ------
　　起動時の正面にくる位置を指定します。Latitude=緯度、Longitude=経度です。
#Latitude 0.0  :[-90,90]
#Longitude 135.0 :[-180,180]

===================================================
　　ここで地球画像のアニメーションと回転を設定します。
----- Initial setup for animation & spin ------
　　起動時のアニメーションを設定します。
　　0:静止状態 1:正転 -1:逆転　です。
　　0と設定した場合は、「a」キーでコマ送りしたり、「スペース」キーで再生を開始したりします。
#Animation 1 :[0: stop, 1: play, -1: reverse play]

アニメーションの再生速度を指定します。大きい数字ほど速い再生速度になります。
#AnimationSpeed 8 :[-30,30]

　　最初のコマでの一時停止の長さを指定します。
#StopAt1stMap 1.0

　　最後のコマでの一時停止の長さを指定します。
#StopAtFinalMap 1.0
　　
　　アニメーションを繰り返す場合は、 1 に 、しない場合は 0 にしてください。
#Repeat 1 

　　起動時に自転する場合は、 1 に 、しない場合は 0 にしてください。
#Spin 0

　　自転の速度を指定します。正の数だと地球の自転の方向、負の数だと逆向きの方向で、
　　絶対値の大きい数ほど自転が早くなります。
　　「1（遅）」→「200（速）」
#SpinSpeed 5 :[-200,200]

　　自転の速度の指定の別な方法ですが、通常は使わないです。
#SpinIntervalSecond 0.005 :[Smaller number gets faster spin.]
===================================================

----- View point ------
　　投影方法の指定です。通常の投影では 0 にしてください。
　　近接投影のプロジェクターで背面から投影する場合は、1 にして、次のEyePositionも調整して
　　ください。
#Perspective 0:[0: orthogonal, 1: perspective]
#EyePosition 80.0

===================================================
　　レイヤーの数を指定します。レイヤーは地球に貼り付ける画像の数で、1 あるいは 2 です。
　　多くのコンテンツでは 1 です。
#NumberOfTextures 1  :[ 1 or 2 ]

　　起動時に２つ目のレイヤーを表示する場合は、1 にします。
#SecondTexture_at_startup 0

----- Image file names ------
　　ここで読み込む画像を指定します
--- Window 1 ---
　　Textureとは、地球画像を出す仮想球体に貼り付ける画像を選択しています
　　TextureNameの後に続く表記は投影したい画像のあるフォルダ名です。
#TextureName data/images/map/map_

　　Screenとは、キャプション（説明文）の画像を選択しています
　　ScreenNameの後に続く表記は投影したい画像のあるフォルダネームです
#ScreenName data/images/screen/screen_

　　２つ目のレイヤーの地球画像とキャプション画像のフォルタ名です。
#SecondTextureName data/images/map_second/map_
#SecondScreenName data/images/screen_second/screen_

　　画像の形式を拡張子で指定します。
　　jpg(JPEG)とppm(PPM)が可能ですが、通常はJPEGを使います。
#TextureSuffix jpg
#SecondTextureSuffix jpg
#ScreenSuffix jpg
#SecondScreenSuffix jpg    

--- Window 2 ---
　　2つ目のウィンドウでの地球画像とキャプション画像のフォルタ名です。
　　2つのウィンドウを使うには”NumberOfEarth 2”という指定をします。
#TextureName2 data/images/map/map_
#ScreenName2 data/images/screen/screen_
#SecondTextureName2 data/images/map_second/map_
#SecondScreenName2 data/images/screen_second/screen_

　　画像の形式を拡張子で指定します。
#TextureSuffix2 jpg
#SecondTextureSuffix2 jpg
#ScreenSuffix2 jpg
#SecondScreenSuffix2 jpg    
----------------------	

　　投映する画像の始まりと終わりの番号を指定しています。
　　以下の設定では画像は0から始まって、連番で9999枚までの投映が可能ということです。
　　連続しないとそこで終わります。
#TextureStart 0
#TextureEnd 9999

===================================================
----- GRID: Latitude & Longitude lines ------
　　緯度経度線（グリッド線）の数、太さを設定します
　　Meridian=子午線/経線、Latitude=緯線
　　数字は、１番目は 0 の場合は、起動時にinit_conf.txtを表示しない、1 の場合は起動時に表示する。
　　2番目が経度線の数、３番目が緯度線の数、４番目が緯度経度線の太さ、４番目が赤道線の太さです。
#MeridianLatitude 0 12 6 3. 5.

　　緯度経度線（グリッド線）の色の指定です。
　　最初の４つで、緯度経度線の色と透明度を0から1で指定します。
　　次の４つで、赤道線の色と透明度を0から1で指定します。
　　色と透明度の指定は、R（レッド）G（グリーン）B（ブルー） alpha（透明度）の順です。
　　たとえば、以下四つの数字が並んでいた場合。
　　0.4 0.5 0.6 0.7
　　R = 0.4, G = 0.5, B = 0.6, alpha = 0.7 
　　と読み込みます。
　　RGB alphaの値ですが、それぞれ、0から1までの間の値を取ることができます。
　　RGBの全部が1のときが白で、
　　RGBの全部が0のときが黒です。 
　　alpha=1の時が、透明度がゼロで、alpha=0の時が、完全な透明です。
#ColorMeridianLatitude 0.7 0.7 0.7 0.7  0.8 0.2 0.2 0.7

===================================================
----- Drawing Pallete & LIne ------
　　お絵描きモードの設定です。
　　カラーパレットの大きさと、場所（X座標、Y座標）の指定です。
#PaletteScaleXY 0.1 2.6 0.51           ###Color palette for drawing: scale, x, y 

　　線の太さと起動時の色と透明度の指定です。
　　色と透明度の指定は、R（レッド）G（グリーン）B（ブルー） alpha（透明度）の順です。
　　たとえば、以下四つの数字が並んでいた場合。
　　0.4 0.5 0.6 0.7
　　R = 0.4, G = 0.5, B = 0.6, alpha = 0.7 
　　と読み込みます。
　　RGB alphaの値ですが、それぞれ、0から1までの間の値を取ることができます。
　　RGBの全部が1のときが白で、
　　RGBの全部が0のときが黒です。 
　　alpha=1の時が、透明度がゼロで、alpha=0の時が、完全な透明です。
#LineThicknessColor 2.5 1. 0.373 0.125 1.0 # Drawing line thickness, R, G, B, alpha

===================================================
----- ARROW MODE: 矢印モード ------  CKK1.23以降
　　指定した場所に指定した向き・長さ・色の矢印を描きます。
　　ファイル名の設定。
#ArrowsName data/plot/arrows/arrows_
#ArrowsSuffix csv
　　矢印を表示しない場合は0にします。
#ArrowsOn 1
　　起動時に矢印を表示しない場合は0にします。
#ArrowsInitDisplay 1

===================================================
----- LINES MODE: 線画モード ------  CKK1.23以降
　　指定した点を線で結んで表示します。
　　ファイル名の設定。
#LinesName data/plot/lines/lines_
#LinesSuffix csv
　　線を表示しない場合は0にします。
#LinesOn 1
　　起動時に線を表示しない場合は0にします。
#LinesInitDisplay 1

===================================================
----- POINT MODE: ポイントモード ------  CKK1.23以降
　　指定した場所に、印（ポイント）を表示します。
　　ファイル名の設定。
#PointsName data/plot/points/points_
#PointsSuffix csv
　　ポイントを表示しない場合は0にします。
#PointsOn 1
　　起動時にポイントを表示しない場合は0にします。
#PointsInitDisplay 1

===================================================
----- Icon ------
　　ここでアイコン（ボタン）の設定をします

　　アイコンの数を指定します。0 にするとアイコンは表示されません。27が最大数で
　　アイコンを１つでも表示する場合は、27にしておきます。
!#Icon 0                                          ### For no Icons
#Icon 27                                      ### Full Icons

　　アイコンの大きさと場所（X座標、Y座標）の指定です。
#IconScaleXY 0.2 2.4 -1.5		       ###Play/Stop
#Icon2ScaleXY 0.2 1.9 -1.75	        ###Initial
#Icon3ScaleXY 0.2 2.15 -1.5		     ###Reverse Play/Stop
#Icon4ScaleXY 0.2 2.4 1.3		       ###Spin/Stop
#Icon5ScaleXY 0.2 2.4 -1.75		      ###1step Forward
#Icon27ScaleXY 0.2 1.9   -1.5		  ###1step Forward & Change Layer
#Icon6ScaleXY 0.2 2.15 -1.75		 ###1step Backward
#Icon7ScaleXY 0.2 2.4 1.05		       ###Latitude/Longitude Lines
#Icon8ScaleXY 0.2 2.4 1.55		       ###Zoom in
#Icon9ScaleXY 0.2 2.15 1.55		      ###Zoom out
#Icon10ScaleXY 0.2 2.15 1.3		      ###North upward
#Icon11ScaleXY 0.2 2.4 -1.25	      ### Fast (Play speed)
#Icon12ScaleXY 0.2 2.15 -1.25	      ### Slow (Play speed)
#Icon13ScaleXY 0.2 2.4 1.95		        ###Quit
#Icon14ScaleXY 0.2 2.15 1.95		  ###Full Screen

#Icon15ScaleXY 0.18 -2.20 1.95		  ###Logo Small
#Icon16ScaleXY 0.18 -1.995 1.95		  ###Logo Large
#Icon17ScaleXY 0.2 -2.65 1.85		  ###Logo Left
#Icon18ScaleXY 0.2 -2.45 1.85		  ###Logo Right
#Icon19ScaleXY 0.2 -2.55  1.65		  ###Logo Down
#Icon20ScaleXY 0.2 -2.55  2.05		  ###Logo Up

#Icon21ScaleXY 0.2 -0.85  -1.75		  ###Switch Layers
#Icon22ScaleXY 0.2 -1.25  -1.75		  ###Screen Show/Hide
#Icon23ScaleXY 0.2 -1.5  -1.75		  ###Icon Show/Hide

#Icon24ScaleXY 0.2 2.4  -0.5	        ###Icon Drawing mode / Rotation mode 
#Icon25ScaleXY 0.2 2.4   -0.75		  ###Icon Erase drawings
#Icon26ScaleXY 0.2 2.4   -1.		   ###Icon Save drawings

--- If don't like to show following icons ---
　　アイコンの大きさを 0 にすると表示されなくなります。ここで、通常使わないアイコンの
　　大きさを 0 にしてあります。
　　それ以外のアイコンも表示させたくない場合は、!を外すと表示されなくなります。

!#IconScaleXY 0. 0. 0. 		        ###Play/Stop
!#Icon2ScaleXY  0. 0. 0. 		   ###Initial
!#Icon3ScaleXY  0. 0. 0. 		   ###Reverse Play/Stop
!#Icon4ScaleXY  0. 0. 0. 		   ###Spin/Stop
!#Icon5ScaleXY  0. 0. 0. 		   ###1step Forward
#Icon27ScaleXY 0. 0. 0. 		  ###1step Forward & Change Layer
!#Icon6ScaleXY 0. 0. 0. 		   ###1step Backward
!#Icon7ScaleXY 0. 0. 0. 	       ###Latitude/Longitude Lines
!#Icon8ScaleXY 0. 0. 0. 		   ###Zoom in
!#Icon9ScaleXY 0. 0. 0. 		   ###Zoom out
!#Icon10ScaleXY 0. 0. 0. 		  ###North upward
!#Icon11ScaleXY 0. 0. 0. 	      ### Fast (Play speed)
!#Icon12ScaleXY 0. 0. 0. 	      ### Slow (Play speed)
!#Icon13ScaleXY 0. 0. 0. 		  ###Quit
#Icon14ScaleXY 0. 0. 0. 		  ###Full Screen

#Icon15ScaleXY 0. 0. 0. 		  ###Logo Small
#Icon16ScaleXY 0. 0. 0. 		  ###Logo Large
#Icon17ScaleXY 0. 0. 0. 		  ###Logo Left
#Icon18ScaleXY 0. 0. 0. 		  ###Logo Right
#Icon19ScaleXY 0. 0. 0. 		  ###Logo Down
#Icon20ScaleXY 0. 0. 0. 		  ###Logo Up

!#Icon21ScaleXY 0. 0. 0. 		  ###Switch Layers
!#Icon22ScaleXY 0. 0. 0. 		  ###Screen Show/Hide
!#Icon23ScaleXY 0. 0. 0. 		  ###Icon Show/Hide

!#Icon24ScaleXY 0. 0. 0. 	       ###Icon Drawing mode / Rotation mode 
!#Icon25ScaleXY 0. 0. 0. 		   ###Icon Erase drawings
!#Icon26ScaleXY 0. 0. 0. 		   ###Icon Save drawings

===================================================
----- Specific layout ------
　　特別な表示の場合のアイコンの大きさと位置の指定です。
 ---- For Icon on bottom ----
　　アイコンをウィンドウの下の方に置く場合です。
!#IconScaleXY 0.2 2.4 -1.25		       ###Play/Stop
!#Icon2ScaleXY 0.2 1.9 -1.75	        ###Initial
!#Icon3ScaleXY 0.2 2.15 -1.25		     ###Reverse Play/Stop
!#Icon4ScaleXY 0.2 1.65 -1.5		       ###Spin/Stop
!#Icon5ScaleXY 0.2 1.9 -1.25		      ###1step Forward
!#Icon6ScaleXY 0.2 1.65 -1.25		 ###1step Backward
!#Icon8ScaleXY 0. 2.4 1.55		       ###Zoom in
!#Icon9ScaleXY 0. 2.15 1.55		      ###Zoom out
!#Icon7ScaleXY 0.2 1.9 -1.5		       ###Latitude/Longitude Lines
!#Icon10ScaleXY 0.2 1.65 -1.75		      ###North upward
!#Icon11ScaleXY 0.2 2.4 -1.5	      ### Fast (Play speed)
!#Icon12ScaleXY 0.2 2.15 -1.5	      ### Slow (Play speed)
!#Icon13ScaleXY 0.2 2.4 -1.75		        ###Quit
!#Icon14ScaleXY 0. 2.15 -1.75		  ###Full Screen
!#Icon21ScaleXY 0.2 -1.87  -1.75		  ###Switch Layers
!#Icon22ScaleXY 0.2 -2.35  -1.75		  ###Screen Show/Hide
!#Icon23ScaleXY 0.2 -2.6  -1.75		  ###Icon Show/Hide

----- For 2 choice Quiz -----
　　２択クイズの場合です。
!#Icon5ScaleXY 1.2 1.5 0.7		      ###1step Forward
!#Icon27ScaleXY 1.2 1.5 -0.5		      ###1step Forward

---- For Operation Window -----
　　２ウィンドウ使い、片方を操作画面に使う場合です。
!#IconScaleXY  0.7 -2.1 0.4			       ###Play/Stop
!#Icon2ScaleXY 0.7 -2.55 -0.35	        ###Initial
!#Icon3ScaleXY 0.7 -3. 0.4		     ###Reverse Play/Stop
!#Icon10ScaleXY 0.7 -2.55 1.15		      ###North upward

===================================================
