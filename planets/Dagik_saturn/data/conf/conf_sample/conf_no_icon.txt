#Icon 27                                      ### Full Icons
#IconScaleXY 0. 0. 0. 		        ###Play/Stop
#Icon2ScaleXY  0. 0. 0. 		   ###Initial
#Icon3ScaleXY  0. 0. 0. 		   ###Reverse Play/Stop
#Icon4ScaleXY  0. 0. 0. 		   ###Spin/Stop
#Icon5ScaleXY  0. 0. 0. 		   ###1step Forward
#Icon27ScaleXY 0. 0. 0. 		  ###1step Forward & Change Layer
#Icon6ScaleXY 0. 0. 0. 		   ###1step Backward
#Icon7ScaleXY 0. 0. 0. 	       ###Latitude/Longitude Lines
#Icon8ScaleXY 0. 0. 0. 		   ###Zoom in
#Icon9ScaleXY 0. 0. 0. 		   ###Zoom out
#Icon10ScaleXY 0. 0. 0. 		  ###North upward
#Icon11ScaleXY 0. 0. 0. 	      ### Fast (Play speed)
#Icon12ScaleXY 0. 0. 0. 	      ### Slow (Play speed)
#Icon13ScaleXY 0. 0. 0. 		  ###Quit
#Icon14ScaleXY 0. 0. 0. 		  ###Full Screen

#Icon15ScaleXY 0. 0. 0. 		  ###Logo Small
#Icon16ScaleXY 0. 0. 0. 		  ###Logo Large
#Icon17ScaleXY 0. 0. 0. 		  ###Logo Left
#Icon18ScaleXY 0. 0. 0. 		  ###Logo Right
#Icon19ScaleXY 0. 0. 0. 		  ###Logo Down
#Icon20ScaleXY 0. 0. 0. 		  ###Logo Up

#Icon21ScaleXY 0. 0. 0. 		  ###Switch Layers
#Icon22ScaleXY 0. 0. 0. 		  ###Screen Show/Hide
#Icon23ScaleXY 0. 0. 0. 		  ###Icon Show/Hide

#Icon24ScaleXY 0. 0. 0. 	       ###Icon Drawing mode / Rotation mode 
#Icon25ScaleXY 0. 0. 0. 		   ###Icon Erase drawings
#Icon26ScaleXY 0. 0. 0. 		   ###Icon Save drawings