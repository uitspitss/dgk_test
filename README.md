# memo

## chrome起動時

- `--allow-file-access-from-files` の起動引数を追加

## index.html

- ロゴ等は `img` 以下に格納。
- index.htmlのwebglを実行して、clickイベントでresourceを読み込んでいく。
- 各惑星の設定値は、それぞれのresourceのconfの値

## conf

- iconを消す時は `#Icon 0` を指定して、その行より下はすべて削除したほうがよい。(優先順位等にバグありそう)
- captionを消すときは、 `#ScreenScaleXY 1 1 0` を有効にして、画像サイズを潰したほうがよい。 `#ScreenScaleXY 0 0 0` は変なログが出続けるのでやめたほうがよい。

## timer

- `state` が `initial` 以外かつ `10秒間` カーソルの移動がない場合、 `#initial` の `state` に戻す。

## console

- `showConsole` が `false` で非表示

## controls(メニュー選択ボタン)

- `showControls` が `false` で、選択後に非表示、`initial` に戻ったときに表示
- `div#controls` のコメント/コメントアウトの切り替えで初期選択時のボタンの表示/非表示

## sample

![sample.png](sample.png)
