===================================================
Initial Configuration file for Chikyu Kuru Kuru
	Configuration line starts with single "#", e.g., #Latitude, #Spin.
===================================================
�ȉ��ACKK�̐ݒ��܂�
�u#�v�Ŏn�܂�s�̓R�}���h�Ƃ��Ď��s���߂�������Ă��܂��B
����ȊO�̕����Ŏn�܂�s�̓R�����g�ł��āA���s�ɂ͉e�����܂���B

�u!# �v�Ŏn�܂�s�́A�Ƃ肠�����g���Ă��Ȃ��ݒ�ł��B�u!�v�������
�ݒ肪�L���ɂȂ�܂��B

�ݒ�t�@�C���̗D�揇�ʂ� lang-�Ŏn�܂��́Aconf.txt�Ainit_conf.txt�ł��B
�������ڂ��ݒ肳��Ă���ꍇ�́A�D�揇�ʂ̍����t�@�C���ł̐ݒ肪
�g���܂��B
lang- �́A���ꂲ�Ƃ̐ݒ�Aconf.txt�̓R���e���c���Ƃ̐ݒ�A
init_conf.txt �͓��e���i�n�[�h�E�F�A�Ȃǁj���Ƃ̐ݒ�Ɏ�Ɏg���܂��B

----- Initial setup for widow size & fullscreen ------
��ʂ̉𑜓x�i�傫���j��ݒ肵�܂��B�����A�c���̏��ł��B
PC�̉�ʃT�C�Y�ɍ��킹��ƁA��ʂ����ς��ɕ\������܂��B
PC�̉�ʂ̃T�C�Y�̓f�X�N�g�b�v�̃v���p�e�B����m�F�ł��܂��B
#WindowSizeXY 1024 768

��ʂ̈ʒu�i�E�B���h�E�̍���p�̍��W�j��ݒ肵�܂��BX���WY���W�̏��ł��B
#WindowPositionXY 0 0

�N�����ɑS��ʕ\���ɂ���ꍇ�� 1 �� �A���Ȃ��ꍇ�� 0 �ɂ��Ă��������B
Windows�ł͑S��ʕ\�������܂������Ȃ��ꍇ�������̂Œ��ӂ��Ă��������B
#FullScreen 0

�Q�E�B���h�E�ڂ̉�ʂ̉𑜓x�A�ʒu�A�S��ʕ\���̐ݒ�ł��B
�Q�E�B���h�E��g���ɂ́A��ŏo�Ă���NumberOfEarth 2 �Ǝw�肵�܂��B
!#WindowSizeXY2  1024 768
!#WindowPositionXY2 1024 0
!#FullScreen2 0

===================================================
----- Misc. ------
�J�[�\����\�����Ȃ��ꍇ�� 1 �� �A����ꍇ�� 0 �ɂ��Ă��������B
�^�b�`�f�B�X�v���C�ő��삷��ꍇ�́A�J�[�\��������ƃX�b�L�����܂��B
#InvisibleCursor 0

�N���b�N���b�N�i�N���b�N���Ȃ��Ă�}�E�X�𓮂����Βn������]����j��
����ꍇ�� 1 �� �A���Ȃ��ꍇ�� 0 �ɂ��Ă��������B
�Q�̃E�B���h�E�g���ꍇ�iNumberOfEarth 2�j�͋��������������Ȃ�̂�
�g��Ȃ��ق����ǂ��ł��B
#ClickLock 0

�}�E�X�Ńh���b�O���Ă�A�n������]���Ȃ��悤�ɂ���ꍇ�� 1 �� �A
���Ȃ��ꍇ�� 0 �ɂ��Ă��������B
#NoMouseDragRotation 0

�v���[���e�[�V�����E�����R���E���[�h�ɂ���ꍇ�� 1 �� �A
���Ȃ��ꍇ�� 0 �ɂ��Ă��������B
�v���[���e�[�V�����E�����R���E���[�h�ł́A���E�̖��L�[���R�}����̐i�ށA�߂�ɂȂ�܂��B
#PresentaionRemoteMode 0 : [ For Presentation Remote Controller: '->'key = a, '<-'key = b ]

�n����񂵂ė��������Ƃ�A���̂܂ܐ����ł��邮���葱����ݒ�ł��B
�Ō�̐����� 1. �ɂ���ƁA���邮���葱���A0. �ɂ���Ɨ������炷���~�܂�܂��B
0.99 �Ƃ��ɂ���ƁA���񂾂�~�܂��Ă����܂��B
#InertiaOfRotation 0.02 0.01 0.01 0. : [Start_criteria, Stop_criteria, Speed of rotation, Damping rate (1= little damping, 0 = no inertia rotation)]

===================================================

-------- Earth Size & Position --------
--- Earth Right, Caption Left ----
�n���摜��E�A�L���v�V������ʂ�i���j�^�[1��ɏo�́j

�E�B���h�E�̐��͂P��
#NumberOfEarth 1

�n���摜�̑傫���̎w��ł��B
#Scale 1.9

�n���摜�̈ʒu�̎w��ł��BX���WY���W�̏��ł��B
#EarthXY 0.3 0.0

�L���v�V�����摜�̑傫���̎w��ł��B
#ScreenScaleXY 4.3 -2.6 1.9

�L���v�V�����摜�ƒn���摜���d�Ȃ����ꍇ�ɁA�ǂ�����ɂ��邩�ł��B
0 �̏ꍇ�͒n���摜����A 1 �̏ꍇ�̓L���v�V�����摜����ɂȂ�܂��B
#ScreenFront 0 :[0:  Caption is behind the Earth. 1: Caption is in front of the Earth.]

�������̉摜������ꍇ�ɉ����ڂ̉摜��\�����Ă��邩����u�o�[�v��
�傫���ƈʒu��w�肵�Ă��܂��B
#TimebarScaleXY .2 -1.6 -1.65

===================================================

----- Initial Latitude & Longitude ------
�N�����̐��ʂɂ���ʒu��w�肵�܂��BLatitude=�ܓx�ALongitude=�o�x�ł��B
#Latitude 0.0  :[-90,90]
#Longitude 135.0 :[-180,180]

===================================================
�����Œn���摜�̃A�j���[�V�����Ɖ�]��ݒ肵�܂��B
----- Initial setup for animation & spin ------
�N�����̃A�j���[�V������ݒ肵�܂��B
0:�Î~��� 1:���] -1:�t�]�@�ł��B
0�Ɛݒ肵���ꍇ�́A�ua�v�L�[�ŃR�}���肵����A�u�X�y�[�X�v�L�[�ōĐ���J�n�����肵�܂��B
#Animation 1 :[0: stop, 1: play, -1: reverse play]

�A�j���[�V�����̍Đ����x��w�肵�܂��B�傫�������قǑ����Đ����x�ɂȂ�܂��B
#AnimationSpeed 8 :[-30,30]

�ŏ��̃R�}�ł̈ꎞ��~�̒�����w�肵�܂��B
#StopAt1stMap 1.0

�Ō�̃R�}�ł̈ꎞ��~�̒�����w�肵�܂��B
#StopAtFinalMap 1.0

�A�j���[�V������J��Ԃ��ꍇ�́A 1 �� �A���Ȃ��ꍇ�� 0 �ɂ��Ă��������B
#Repeat 1

�N�����Ɏ��]����ꍇ�́A 1 �� �A���Ȃ��ꍇ�� 0 �ɂ��Ă��������B
#Spin 1

���]�̑��x��w�肵�܂��B���̐����ƒn���̎��]�̕����A���̐����Ƌt�����̕����ŁA
��Βl�̑傫�����قǎ��]�������Ȃ�܂��B
�u-200�i�x�j�v���u200�i���j�v
#SpinSpeed 2 :[-200,200]

���]�̑��x�̎w��̕ʂȕ��@�ł����A�ʏ�͎g��Ȃ��ł��B
#SpinIntervalSecond 0.005 :[Smaller number gets faster spin.]
===================================================

----- View point ------
���e���@�̎w��ł��B�ʏ�̓��e�ł� 0 �ɂ��Ă��������B
�ߐړ��e�̃v���W�F�N�^�[�Ŕw�ʂ��瓊�e����ꍇ�́A1 �ɂ��āA����EyePosition��������
���������B
#Perspective 0:[0: orthogonal, 1: perspective]
#EyePosition 80.0

===================================================
���C���[�̐���w�肵�܂��B���C���[�͒n���ɓ\��t����摜�̐��ŁA1 ���邢�� 2 �ł��B
�����̃R���e���c�ł� 1 �ł��B
#NumberOfTextures 1  :[ 1 or 2 ]

�N�����ɂQ�ڂ̃��C���[��\������ꍇ�́A1 �ɂ��܂��B
#SecondTexture_at_startup 0

----- Image file names ------
�����œǂݍ��މ摜��w�肵�܂�
--- Window 1 ---
Texture�Ƃ́A�n���摜��o�����z���̂ɓ\��t����摜��I��Ă��܂�
TextureName�̌�ɑ����\�L�͓��e�������摜�̂���t�H���_���ł��B
#TextureName data/images/map/map_

Screen�Ƃ́A�L���v�V�����i�����j�̉摜��I��Ă��܂�
ScreenName�̌�ɑ����\�L�͓��e�������摜�̂���t�H���_�l�[���ł�
#ScreenName data/images/screen/screen_

�Q�ڂ̃��C���[�̒n���摜�ƃL���v�V�����摜�̃t�H���^���ł��B
#SecondTextureName data/images/map_second/map_
#SecondScreenName data/images/screen_second/screen_

�摜�̌`����g���q�Ŏw�肵�܂��B
jpg(JPEG)��ppm(PPM)���\�ł����A�ʏ��JPEG��g���܂��B
#TextureSuffix jpg
#SecondTextureSuffix jpg
#ScreenSuffix jpg
#SecondScreenSuffix jpg

--- Window 2 ---
2�ڂ̃E�B���h�E�ł̒n���摜�ƃL���v�V�����摜�̃t�H���^���ł��B
2�̃E�B���h�E��g���ɂ́hNumberOfEarth 2�h�Ƃ����w���܂��B
#TextureName2 data/images/map/map_
#ScreenName2 data/images/screen/screen_
#SecondTextureName2 data/images/map_second/map_
#SecondScreenName2 data/images/screen_second/screen_

�摜�̌`����g���q�Ŏw�肵�܂��B
#TextureSuffix2 jpg
#SecondTextureSuffix2 jpg
#ScreenSuffix2 jpg
#SecondScreenSuffix2 jpg
----------------------

���f����摜�̎n�܂�ƏI���̔ԍ���w�肵�Ă��܂��B
�ȉ��̐ݒ�ł͉摜��0����n�܂��āA�A�Ԃ�9999���܂ł̓��f���\�Ƃ������Ƃł��B
�A�����Ȃ��Ƃ����ŏI���܂��B
#TextureStart 0
#TextureEnd 9999

===================================================
-------- 1 Window --------
--- Earth Right, Caption Left ----
�n���摜��A�L���v�V������ʂ�E�ɏ������\�����܂��i���j�^�[1��ɏo�́j�B
!#NumberOfEarth 1
!#Scale 1.9
!#EarthXY 0.3 0.0
!#ScreenScaleXY 4.3 -2.6 1.9
!#ScreenFront 0 :[0:  Caption is behind the Earth. 1: Caption is in front of the Earth.]
!#TimebarScaleXY .2 -1.6 -1.65

--- Earth Center, Caption on the Earth ----
�n���摜�𒆉��A�L���v�V������ʂ�n���摜�̏�ɕ\�����܂��i���j�^�[1��ɏo�́j�B
!#NumberOfEarth 1
!#Scale 1.9
!#EarthXY 0.0 0.0

!#ScreenScaleXY 3. -0.5 1.    : [Caption is center.]
!!#ScreenScaleXY 6 -1.3 -1.    : [Caption is in lower part.]
!#ScreenFront 1  :[0:  Caption is behind the Earth. 1: Caption is in front of the Earth.]
!#TimebarScaleXY .2 -2.65 -1.65

--- No Earth, Caption Only ----
�n���摜�͕\�����Ȃ��ŁA�L���v�V�����摜������\�����܂��i���j�^�[1��ɏo�́j�B
!#NumberOfEarth 1
!#Scale 0.
!#EarthXY 0.3 0.0
!#NumberOfEarth 1
!#ScreenScaleXY 3.94 -3.12 1.97
!#ScreenFront 0  :[0:  Caption is behind the Earth. 1: Caption is in front of the Earth.]
!#TimebarScaleXY .17 -3.1 -1.75

-------- Backward projection ---------
�w�ʂ���ߐړ��e����ꍇ�̐ݒ�ł��B
!#Perspective 1 :[0: orthogonal, 1: perspective]
!#EarthXY 0. 0.0
!#ScreenScaleXY 3. -0.5 1.    : [Caption is center.]
!!#ScreenScaleXY 6 -1.3 -1.    : [Caption is in lower part.]
!#ScreenFront 1  :[0:  Caption is behind the Earth. 1: Caption is in front of the Earth.]
!#TimebarScaleXY .15 -2.5 -1.65

�����Y�̓����ɂ���ē��e�͈͂��Ⴄ���߁AEeyPosition��Scale�𒲐߂��܂��B
degree=�p�x�ł�
..... 95 degree (Minimum for Backward projection) .....
!#EyePosition -9.
!#Scale 1.9
..... 100 degree .....
!#EyePosition -7.
!#Scale 1.9
..... 110 degree .....
!#EyePosition -4.
!#Scale 1.75
..... 130 degree .....
!#EyePosition -2.
!#Scale 1.41
..... 140 degree .....
!#EyePosition -1.5
!#Scale 1.195
..... 155 degree (Maximum for Backward projection).....
!#EyePosition -.75
!#Scale .702
..... 110 degree (for World Eye by Gakken Sta:ful) .....
!#EyePosition -4.
!#Scale 2.04
........ Cannot use "EarthSizeSynch 0" for 2 windows & backward projection
!#EarthSizeSynch 1    ### Size sync

===================================================
-------- 2 Windows ---------
�ȉ��̓f���A�����j�^�[�����i�Q�̃E�B���h�E�ł̕\���j�̐ݒ�ł��B

--- for 2 Earth & 2 Textures for 2 parameters ---
�Q�̒n���摜�̓��e�ł��B�Q�̈Ⴄ�f�[�^�̕\���Ɏg���܂��B
!#NumberOfEarth 2
!#TextureOfEarth2 1
!#EarthRotationSynch 1  ### Rotation Sync
!#EarthSizeSynch 1    ### Size sync
!#Earth2AngleAxisXY 0.0 0.0 1.0 ### Angle between two Earths.
!#ScreenScaleXY2 4.3 -2.6 1.9
!#TextureName2 data/images_2/map/map_
!#ScreenName2 data/images_2/screen/screen_
!#SecondTextureName2 data/images_2/map_second/map_
!#SecondScreenName2 data/images_2/screen_second/screen_

--- for Earth window and Control window ---
�Е��̃E�B���h�E�ɒn���摜�A�Е��̃E�B���h�E�ɂ͑����ʂ�\�����܂��B
!#NumberOfEarth 2
!#TextureOfEarth2 1
!#EarthSizeSynch 0
!#EarthRotationSynch 1  ### Rotation Sync
!#Scale 0.75
!#Scale2 1.9
!#EarthXY  -0.5 0.0
!#Earth2XY 0.0 0.0
!#ScreenScaleXY 4.3 -1.0 1.9
!#ScreenScaleXY2 3. -0.5 1.    : [Caption is center.]
!#ScreenFront 1  :[0:  Caption is behind the Earth. 1: Caption is in front of the Earth.]
!#TimebarScaleXY .2 -2.65 -1.65

===================================================
----- GRID: Latitude & Longitude lines ------
�ܓx�o�x���i�O���b�h���j�̐��A������ݒ肵�܂�
Meridian=�q�ߐ�/�o���ALatitude=�ܐ�
�����́A�P�Ԗڂ� 0 �̏ꍇ�́A�N������init_conf.txt��\�����Ȃ��A1 �̏ꍇ�͋N�����ɕ\������B
2�Ԗڂ��o�x���̐��A�R�Ԗڂ��ܓx���̐��A�S�Ԗڂ��ܓx�o�x���̑����A�S�Ԗڂ��ԓ����̑����ł��B
#MeridianLatitude 0 12 6 3. 5.

�ܓx�o�x���i�O���b�h���j�̐F�̎w��ł��B
�ŏ��̂S�ŁA�ܓx�o�x���̐F�Ɠ����x��0����1�Ŏw�肵�܂��B
���̂S�ŁA�ԓ����̐F�Ɠ����x��0����1�Ŏw�肵�܂��B
�F�Ɠ����x�̎w��́AR�i���b�h�jG�i�O���[���jB�i�u���[�j alpha�i�����x�j�̏��ł��B
���Ƃ��΁A�ȉ��l�̐���������ł����ꍇ�B
0.4 0.5 0.6 0.7
R = 0.4, G = 0.5, B = 0.6, alpha = 0.7
�Ɠǂݍ��݂܂��B
RGB alpha�̒l�ł����A���ꂼ��A0����1�܂ł̊Ԃ̒l���邱�Ƃ��ł��܂��B
RGB�̑S����1�̂Ƃ����z���C�g�ŁA
RGB�̑S����0�̂Ƃ����u���b�N�ł��B
alpha=1�̎����A�����x���[���ŁAalpha=0�̎����A���S�ȓ����ł��B
#ColorMeridianLatitude 0.7 0.7 0.7 0.7  0.8 0.2 0.2 0.7

===================================================
----- Drawing Pallete & LIne ------
���G�`�����[�h�̐ݒ�ł��B
�J���[�p���b�g�̑傫���ƁA�ꏊ�iX���W�AY���W�j�̎w��ł��B
#PaletteScaleXY 0.1 2.6 0.51           ###Color palette for drawing: scale, x, y

���̑����ƋN�����̐F�Ɠ����x�̎w��ł��B
�F�Ɠ����x�̎w��́AR�i���b�h�jG�i�O���[���jB�i�u���[�j alpha�i�����x�j�̏��ł��B
���Ƃ��΁A�ȉ��l�̐���������ł����ꍇ�B
0.4 0.5 0.6 0.7
R = 0.4, G = 0.5, B = 0.6, alpha = 0.7
�Ɠǂݍ��݂܂��B
RGB alpha�̒l�ł����A���ꂼ��A0����1�܂ł̊Ԃ̒l���邱�Ƃ��ł��܂��B
RGB�̑S����1�̂Ƃ����z���C�g�ŁA
RGB�̑S����0�̂Ƃ����u���b�N�ł��B
alpha=1�̎����A�����x���[���ŁAalpha=0�̎����A���S�ȓ����ł��B
#LineThicknessColor 2.5 1. 0.373 0.125 1.0 # Drawing line thickness, R, G, B, alpha

===================================================
----- Icon ------
�����ŃA�C�R���i�{�^���j�̐ݒ��܂�

�A�C�R���̐���w�肵�܂��B0 �ɂ���ƃA�C�R���͕\������܂���B27���ő吔��
�A�C�R����P�ł�\������ꍇ�́A27�ɂ��Ă����܂��B
#Icon 0                                          ### For no Icons
