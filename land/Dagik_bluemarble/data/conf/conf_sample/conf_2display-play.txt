#Icon 10
#IconScaleXY  0.7 -2.1 0.4			       ###Play/Stop
#Icon2ScaleXY 0.7 -2.55 -0.35	        ###Initial
#Icon3ScaleXY 0.7 -3. 0.4		     ###Reverse Play/Stop
#Icon4ScaleXY 0. 2.4 1.3		       ###Spin/Stop
#Icon5ScaleXY 0. -2.1 0.4		      ###1step Forward
#Icon27ScaleXY 0. 2.4   -0.25		  ###1step Forward & Change Layer
#Icon6ScaleXY 0. -3. 0.4			 ###1step Backward
#Icon7ScaleXY 0. 2.4 1.05		       ###Latitude/Longitude Lines
#Icon8ScaleXY 0. 2.4 1.55		       ###Zoom in
#Icon9ScaleXY 0. 2.15 1.55		      ###Zoom out
#Icon10ScaleXY 0. 2.15 1.3		      ###North upward
#Icon10ScaleXY 0.7 -2.55 1.15		      ###North upward
