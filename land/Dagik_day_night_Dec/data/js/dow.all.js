// 2015-05-16 : 読み込み中に「戻る」ボタン、「リロード」ボタン追加 by Saito-A
// 2015-07-04：経度指定の間違いを修正：-Longitude-90deg. を表示していた(135Eは同じ）by Saito-A  
//
var DOW = { VERSION: '0.1b' };
if ( typeof module === 'object' ) {	module.exports = DOW; }

// 設定ファイルの画面座標系における画面上辺のY座標（中心が原点なので高さの半分でもある）
DOW.WTOP = 2.0;

// 静止画連番アニメーションの標準（speed=8）のフレームレート
DOW.CKK_STD_FPS = 5.0;
// 地球の表示位置の計算方法をCKKに合わせる
DOW.CKK_EARTHXY_POLICY = true;
// 地球の回転操作の方法をCKKに合わせる
DOW.CKK_ROTATION_POLICY = false;

// エラーコードとメッセージ表示文字列の定義
DOW.MESSAGES_JA = {
// メインメッセージ
M0001: '設定ファイルのロード中(Loading)<p><button onclick="window.history.back();">戻る(back)</button><button onclick="window.location.reload( false );">リロード(reload)</button></p>',
M0002: 'リソースのロード中(Loading)<p><button onclick="window.history.back();">戻る(back)</button><button onclick="window.location.reload( false );">リロード(reload)</button></p>',
M1001: 'エラーが発生しました(Errors)<p><button onclick="window.history.back();">戻る(back)</button><button onclick="window.location.reload( false );">リロード(reload)</button></p>',
// 一般的なエラー
E0001: 'ブラウザがDagikEarthに対応していません(WebGL)',
// 設定ファイル関連のエラー
E0011: '設定ファイルがありません',
E0012: '設定ファイルが読み込めません',
E0013: '未定義のパラメータです',
E0014: 'パラメータの数が足りません',
E0015: 'パラメータは数値でなければなりません',
E0016: 'パラメータが不正な値です',
E0018: 'Web版では正しい(9999以外の)TextureEndを明示してください',
E0019: '設定ファイルの処理中にエラーが起きました',
// リソース関連のエラー
E0101: '画像を読み込めません',
E0102: '書式に誤りがあります',
W0101: '画像を読み込めません',
// 
I9999: '参考',
};


DOW.MESSAGES = DOW.MESSAGES_JA;

// 実行時に無視するエラー
DOW.IGNORE_ERRORS = [
  'E0011', // 設定ファイルが無くてもエラーとしない
  'E0012', // 設定ファイルが読み込めなくてもエラーとしない
  'E0013', // 定義されていない設定パラメータでもエラーとしない
  ];


DOW.ICONS = [
  ['IconScaleXY',  	'forward_play', 'play.png', 'pause.png'],
  ['Icon2ScaleXY', 	'initialize',   'initial.png'],
  ['Icon3ScaleXY', 	'reverse_play', 'r_play.png', 'pause.png'],
  ['Icon4ScaleXY', 	'auto_spin', 		'spin.png', 'stop_spin.png'],
  ['Icon5ScaleXY', 	'step_forward', 'forward.png'],
  ['Icon6ScaleXY', 	'step_backward','back.png'],
  ['Icon7ScaleXY', 	'show_grid', 		'line.png', 'delete_line.png'],
  ['Icon8ScaleXY', 	'zoom_in', 			'zoomin.png'],
  ['Icon9ScaleXY', 	'zoom_out', 		'zoomout.png'],
  ['Icon10ScaleXY', 'reset_north', 	'north.png'],
  ['Icon11ScaleXY', 'play_fast', 		'fast.png'],
  ['Icon12ScaleXY', 'play_slow', 		'slow.png'],
  ['Icon13ScaleXY', 'quit', 				'quit.png'],
  ['Icon14ScaleXY', 'fullscreen', 	'normalscreen.png', 'fullscreen.png'],
  ['Icon15ScaleXY', 'screen_small', 'logo_small.png'],
  ['Icon16ScaleXY', 'screen_large', 'logo_large.png'],
  ['Icon17ScaleXY', 'screen_left', 	'logo_left.png'],
  ['Icon18ScaleXY', 'screen_right', 'logo_right.png'],
  ['Icon19ScaleXY', 'screen_down', 	'logo_down.png'],
  ['Icon20ScaleXY', 'screen_up', 		'logo_up.png'],
  ['Icon21ScaleXY', 'layer', 				'layer1.png', 'layer2.png'],
  ['Icon22ScaleXY', 'show_screen', 	'screen_on.png', 'screen_off.png'],
  ['Icon23ScaleXY', 'show_drawing', 'icon_on.png', 'icon_off.png'],
  ['Icon24ScaleXY', 'draring_pen',	'drawing_off.png', 'drawing_on.png'],
  ['Icon25ScaleXY', 'drawing_erase','drawing_erase.png'],
  ['Icon26ScaleXY', 'drawing_save', 'drawing_save.png'],
  ['Icon27ScaleXY', 'change_layer', 'forward-changelayer.png']
  ];

DOW.KEY_ACTION = {

  'ESCAPE': 'fullscreen_off',
  'UP_ARROW': 'up_arrow_key',
  'DOWN_ARROW': 'down_arrow_key',
  'LEFT_ARROW': 'left_arrow_key',
  'RIGHT_ARROW': 'right_arrow_key',

  'q': 'quit',
  'Q': 'quit',
  'a': 'step_forward',
  'A': 'step_forward10',
  'c': 'show_screen',
  'o': 'layer',
  'b': 'step_backward',
  'B': 'step_backward10',
  'r': 'reverse_play',
  'L': 'zoom_in',
  'l': 'zoom_in_petit',
  'H': 'zoom_out',
  'h': 'zoom_out_petit',
  's': 'auto_spin',
  'm': 'show_grid',
  'f': 'fullscreen',
  'i': 'initialize',
  'I': 'first_frame',
  'J': 'last_frame',
  'n': 'reset_north',
  'C': 'hide_all_icons',
  ' ': 'pause_or_play', 	// 0x20,
  '+': 'play_fast', // 0x2b,
  '-': 'play_slow', // 0x2d,
  '>': 'spin_fast', // 0x3e,
  '<': 'spin_slow', // 0x3c,
  '1': 'screen_small',
  '2': 'screen_large',
  '3': 'screen_left',
  '4': 'screen_right',
  '5': 'screen_down',
  '6': 'screen_up',
  // お絵描きモード,
  'd': 'show_drawing',
  'p': 'drawing_pen',
  'e': 'drawing_erase',
  'E': 'drawing_undo',

  // 開発支援
  'D': 'toggle_console',
  'F': 'loading_console',
};

DOW.log = function( message ) { console.log ( message ); }

DOW.Console = function( app, parent ) {
  this.type = 'Console';

  this.domElement = document.createElement('div');
  this.domElement.id = 'dow_console';
  parent.appendChild(this.domElement);

  this.domElement.innerHTML = "<div class='progress'><canvas width=60 height=60 style='display: inline;'></canvas><span class='message'></span></div><div class='error'></div><div class='details'></div>";
  
  this.progressElement = this.domElement.childNodes[0];
  this.spinner = this.progressElement.childNodes[0];
  this.progressMessage = this.progressElement.childNodes[1];
  this.messageElement = this.domElement.childNodes[1];
  this.detailsElement = this.domElement.childNodes[2];

  // Detailsに追加されたメッセージにエラーが含まれるかどうか
  this.hasError = false;

  this.phase = 0;
  this.progressing = true;
  var _this = this;
  window.setInterval( function() { _this.update_spinner(); }, 100 );
};

DOW.Console.prototype = {

  show: function() { this.domElement.style.display = 'block'; },
  hide: function() { this.domElement.style.display = 'none'; },
  toggle: function() { this.domElement.style.display = (this.domElement.style.display == 'none') ? 'block' : 'none'; },
  loading: function() { this.showProgress( 'テストで回ってます' ); },

  showProgress: function( message ) {
    this.progressElement.style.display = 'block';
    this.progressMessage.innerHTML = message;
    this.show();
  },

  hideProgress: function() { this.progressElement.style.display = 'none'; },

  showError: function( message ) {
    this.progressElement.style.display = 'none';
    this.messageElement.innerHTML = message;
    this.show();
  },

  addDetail: function( code, message ) {
    var etype = code.charAt(0);
    if (etype == 'E') {
      if (DOW.IGNORE_ERRORS.indexOf(code) < 0) this.hasError = true;
      else etype = 'W';
    }
    this.detailsElement.innerHTML += '<div class=detail_'+etype+'>'+DOW.MESSAGES[code]+': '+message+"</div>\n";
  },

  clear: function() {
    this.progressMessage.innerHTML = '';
    this.messageElement.innerHTML = ''
    this.detailsElement.innerHTML = '';
    this.hasError = false;
  },

  update_spinner: function() {
    if (this.domElement.style.display == 'none' || this.progressElement.style.display == 'none') return;

    var size = this.spinner.height;
    var frequency = 0.5;
    var pi2 = Math.PI * 2;
    this.phase += frequency * pi2 * 0.1;
    while (this.phase > pi2) this.phase -= pi2;

    var gc = this.spinner.getContext('2d');
    //gc.save();
    gc.clearRect(0, 0, size, size);
    gc.strokeStyle = 'rgba(255, 255, 255, 0.8)';
    gc.lineWidth = 5;
    gc.beginPath();
    if (! this.progressing) {
      gc.arc(size/2, size/2, size/3, 0, pi2, true);
    } else {
      gc.arc(size/2, size/2, size/3, this.phase, this.phase+Math.PI, true);
      gc.stroke();
      gc.beginPath();
      gc.arc(size/2, size/2, size/3, -this.phase*2, -this.phase*2+Math.PI, false);
      gc.lineWidth = 1;
    }
    gc.stroke();
    //gc.restore();
  },

};

/***

DOW.Config: Dagik_Earthの設定ファイルを読んで保持する。

使い方：

function callback() {
  // 読み込みが完了した時に実行する内容
}

var dow = new DOW.Config(['data/conf/init_conf.txt', 'data/conf/conf.txt', ...], callback);

// 設定ファイルの読み込みにエラーがあると、配列 dow.errors にエラーの内容が入る。

// 設定値はパラメータ名で読み出せる。
var lon = dow.Longitude;                  // 単一の値は直接
var window_size_x = dow.WindowSize[0];    // 複数の値は配列として

***/


DOW.Config = function( app ) {
  this.type = 'Config';

  this.app = app;
  this.has_error;

  // Set initial values.
  for (var key in DOW.Config.PARAMETER_DEFINITION) {
    this[key] = DOW.Config.PARAMETER_DEFINITION[key][0];
  }

};

// 設定値を後処理するためのよく使う関数
function force_bool(val) { return val === 1; }		// 0,1を真偽値として扱う

// 定義されたパラメータの一覧
// ParameterName: [ 初期値, 値の後処理をする関数 ]
DOW.Config.PARAMETER_DEFINITION = {
  // ----- 画面の設定 ------
  WindowSizeXY:   [[800, 600]],            // ウィンドウのサイズ(Mac版のみ)
  WindowPositionXY:  [[0, 0]],              // ウィンドウの位置(Mac版のみ)
  FullScreen:     [0,      force_bool ],    // フルスクリーン表示
  Perspective:     [0,      force_bool ],    // [0: orthogonal, 1: perspective] 投影方法の指定
  EyePosition:     [20.0],                  // 視点の引き距離(地球の半径より小さい場合は背面投影)
  // ----- アニメーションの設定 ------
  Animation:       [0,      function(val) { // [0: stop, 1: play, -1: reverse play]
      return (val != 1 && val != -1) ? 0 : val;
    }],
  AnimationSpeed: [8],                     // [-30,30]
  StopAt1stMap:   [0.2],                   // アニメーションの最初の画像での待ち時間
  StopAtFinalMap:  [0.0],                  // アニメーションの最後の画像での待ち時間
  Repeat:         [0,      force_bool ],    // アニメーションをループ再生する
  Spin:           [0,      force_bool ],    // 自動で自転する
  SpinSpeed:       [6],                     // [-200,200] 自転速度
  // ----- 地球の位置と大きさ, 起動時の視点 ------
  EarthXY:         [[0.0, 0.0]],
  Scale:           [1.0],                  // ez < 0の場合はscaleを負に(鏡像処理？)
  Latitude:       [0.0],                  // [-90,90] 正面の経度
  Longitude:       [0.0],                  // [-180,180] 正面の緯度
  // ----- キャプション(Screen)の位置と大きさ、タイムバー ------
  ScreenOn:        [1,      force_bool ],    // キャプションを表示する
  ScreenScaleXY:   [[3.0, -2.5, 1.0]],      // キャプションのサイズと位置
  ScreenFront:     [0,      force_bool ],    // [0: 地球が手前, 1: キャプションが手前] 地球とキャプションの前後関係
  TimebarScaleXY: [[1.0, -1.0, -1.0]],    // タイムバーのサイズと位置
  // ----- 画像ファイルの指定 ------
  TextureName:             [''],       // マッピング画像のパスとファイル名(ベース部分)
  TextureSuffix:           [''],       // マッピング画像のサフィックス
  ScreenName:              [''],       // キャプション画像のパスとファイル名(ベース部分)
  ScreenSuffix:            [''],       // キャプション画像のサフィックス
  SecondTextureName:       [''],       // 第２マッピング画像のパスとファイル名(ベース部分)
  SecondTextureSuffix:     [''],       // 第２マッピング画像のサフィックス
  SecondScreenName:        [''],       // 第２キャプション画像のパスとファイル名(ベース部分)
  SecondScreenSuffix:      [''],       // 第２キャプション画像のサフィックス
  TextureStart:            [0],        // 連番ファイルの先頭の数値
  TextureEnd:              [0],        // 連番ファイルの最後の数値
  SecondTexture_at_startup:  [0,    force_bool ],  // 起動時に第２画像も表示
  /* DOW拡張設定*/
  // 代替ビデオファイル（廃止予定）
  VideoTextureName:        [''],        // マッピング画像の代替ビデオファイル。
  VideoScreenName:         [''],        // キャプション画像の代替ビデオファイル。
  SecondVideoTextureName:  [''],        // 第２マッピング画像の代替ビデオファイル。
  SecondVideoScreenName:   [''],        // 第２キャプション画像の代替ビデオファイル。
  // 代替素材ファイル（推奨）
  AltTextureName:          [['', '']],  // マッピング画像の代替素材。
  AltScreenName:           [['', '']],  // キャプション画像の代替素材。
  AltSecondTextureName:    [['', '']],  // 第２マッピング画像の代替素材。
  AltSecondScreenName:     [['', '']],  // 第２キャプション画像の代替素材。
  NumberOfVideoFrames:     [0],         // ビデオテクスチャーの場合のコマ数指定（コマ送り用）
  // 自動素材更新(暫定)
  AutoReloadInterval:      [60],        // 画像素材の自動更新間隔（分）
  TextureAutoReload:       [0,      force_bool],    // マッピング画像を自動更新するかどうか
  ScreenAutoReload:        [0,      force_bool],    // キャプション画像を自動更新するかどうか
  SecondTextureAutoReload: [0,      force_bool],    // 第２マッピング画像を自動更新するかどうか
  SecondScreenAutoReload:  [0,      force_bool],    // 第２キャプション画像を自動更新するかどうか

  // ----- 二つ目の地球 ------
  NumberOfEarth:   [1,      function(val) {  // 表示する地球の個数（1 または 2）
      return (val != 2 ) ? 1 : 2;
    }],
  WindowSizeXY2:   [[-9999, -9999]],        // 指定すると第二モニターに表示？
  WindowPositionXY2:  [[-9999, -9999]],
  FullScreen2:     [0],                    // １なら起動時に2つ目のWindowをfull screenにする}: ckk1.21で追加
  Earth2XY:       [[0.0, 0.0]],            // 二つ目の地球の位置(1つ目の地球からの相対)
  Latitude2:       [0.0],                   // [-90,90] 起動時の正面の緯度(1つ目の地球に同期させない場合)
  Longitude2:     [0.0],                   // [-180,180] 起動時の正面の経度(1つ目の地球に同期させない場合)
  Scale2:         [1.0],                   // 二つ目の地球のサイズ(1つ目の地球に同期させない場合)
  /* 処理の意図確認:
    if (init_scale_factor2 > ez - nearlimit) init_scale_factor = ez-nearlimit;
    scale_factor2 = init_scale_factor2;
   */
  ScreenScaleXY2:      [[3, -2.5, 1.0]],    // 二つ目の地球用のキャプションのサイズと位置
  TextureOfEarth2:     [0,    force_bool ],  // 二つ目の地球は別のテクスチャーを使う
  EarthRotationSynch:  [1,    force_bool ], // 回転を1つ目の地球に同期
  EarthSizeSynch:      [1,    force_bool ], // 大きさを1つ目の地球に同期
  Earth2AngleAxisXY:   [[0.0, 0.0, 1.0]],  // 1つ目の地球との角度差

  NumberOfTextures:         [1,    function(val) {         // レイヤー数（1または2）2015.03.ckk 追加仕様
      return (val != 2 ) ? 1 : 2;
    }],
  TextureName2:             [''],        // 一つ目の地球と同様の設定（以下同じ）
  TextureSuffix2:           [''],
  ScreenName2:              [''],
  ScreenSuffix2:            [''],
  SecondTextureName2:       [''],
  SecondTextureSuffix2:     [''],
  SecondScreenName2:        [''],
  SecondScreenSuffix2:      [''],
  /* DOW拡張設定*/
  // 代替ビデオファイル（廃止予定）
  VideoTextureName2:        [''],        // マッピング画像の代替ビデオファイル。
  VideoScreenName2:         [''],        // キャプション画像の代替ビデオファイル。
  SecondVideoTextureName2:  [''],        // 第２マッピング画像の代替ビデオファイル。
  SecondVideoScreenName2:   [''],        // 第２キャプション画像の代替ビデオファイル。
  // 代替素材ファイル（推奨）
  AltTextureName2:          [['', '']],  // マッピング画像の代替素材。
  AltScreenName2:           [['', '']],  // キャプション画像の代替素材。
  AltSecondTextureName2:    [['', '']],  // 第２マッピング画像の代替素材。
  AltSecondScreenName2:     [['', '']],  // 第２キャプション画像の代替素材。
  // 自動素材更新（暫定）
  TextureAutoReload2:       [0,      force_bool],    // マッピング画像を自動更新するかどうか
  ScreenAutoReload2:        [0,      force_bool],    // キャプション画像を自動更新するかどうか
  SecondTextureAutoReload2: [0,      force_bool],    // 第２マッピング画像を自動更新するかどうか
  SecondScreenAutoReload2:  [0,      force_bool],    // 第２キャプション画像を自動更新するかどうか

  // ----- 操作・GUI関連の設定 ------
  InvisibleCursor:     [0,    force_bool ],  // カーソルを非表示にする
  NoMouseDragRotation:[0,    force_bool ],  // マウスのドラッグで地球を回転させない
  ClickLock:          [0,    force_bool ],  // マウスの移動強制的に送る？
  Icon:               [0],                 // [0: For no Icons, 13: Without Fullscreen, 14: Full Icons]
  IconScaleXY:        [[0, NaN, NaN]], //Play/Stop
  Icon2ScaleXY:       [[0, NaN, NaN]], //Initial
  Icon3ScaleXY:       [[0, NaN, NaN]], //Reverse Play/Stop
  Icon4ScaleXY:       [[0, NaN, NaN]], //Spin/Stop
  Icon5ScaleXY:       [[0, NaN, NaN]], //1step Forward
  Icon6ScaleXY:       [[0, NaN, NaN]], //1step Backward
  Icon7ScaleXY:       [[0, NaN, NaN]], //Latitude/Longitude Lines
  Icon8ScaleXY:       [[0, NaN, NaN]], //Zoom in
  Icon9ScaleXY:       [[0, NaN, NaN]], //Zoom out
  Icon10ScaleXY:      [[0, NaN, NaN]], //North upward
  Icon11ScaleXY:      [[0, NaN, NaN]], // Fast (Play speed)
  Icon12ScaleXY:      [[0, NaN, NaN]], // Slow (Play speed)
  Icon13ScaleXY:      [[0, NaN, NaN]], //Quit
  Icon14ScaleXY:      [[0, NaN, NaN]], //Full Screen

  Icon15ScaleXY:      [[0, NaN, NaN]], // Logo Small
  Icon16ScaleXY:      [[0, NaN, NaN]], // Logo Large
  Icon17ScaleXY:      [[0, NaN, NaN]], // Logo Left
  Icon18ScaleXY:      [[0, NaN, NaN]], // Logo Right
  Icon19ScaleXY:      [[0, NaN, NaN]], // Logo Down
  Icon20ScaleXY:      [[0, NaN, NaN]], // Logo Up

  Icon21ScaleXY:      [[0, NaN, NaN]], // Switch Layers
  Icon22ScaleXY:      [[0, NaN, NaN]], // Screen Show/Hide

  Icon23ScaleXY:      [[0, NaN, NaN]], // Icon Show/Hide(お絵かき？)
  Icon24ScaleXY:      [[0, NaN, NaN]], // お絵描きモードのon off
  Icon25ScaleXY:      [[0, NaN, NaN]], // お絵描き消しゴム (erase drawings)
  Icon26ScaleXY:      [[0, NaN, NaN]], // お絵描き保存 (save drawings)

  Icon27ScaleXY:      [[0, NaN, NaN]], // 

  PresentationRemoteMode:  [0,   force_bool ],  // 左右の矢印をコマ送りの前後にする。（2015.03 ckk 追加機能）
  PresentaionRemoteMode:   [0,   force_bool ],  // Typo?

  // ----- 緯線経線のグリッド表示 ------
  // [緯線経線を表示するかどうか, 経線の数, 緯線の数, 緯線経線の太さ, 赤道の太さ]
  MeridianLatitude:   [[0, 12, 6, 3., 5.],    function(val) {
      val[0] = force_bool(val[0]);
      return val;
    }],
  // [緯線経線の色(RGBA), 赤道の色(RGBA)]
  ColorMeridianLatitude: [[1, 1, 1, 1,  0.8, 0.2, 0.2, 1.0]],

  // ----- その他 ------
  SpinIntervalSecond: [0.0001],				// 自転動作の時間間隔[秒]
  InertiaOfRotation:   [[0.1, 0.01, 0.04, 0.995]], // [Start_criteria, Stop_criteria, Speed of rotation, Damping rate (1= no damping, 0 = no inertia rotation)]

  // ----- お絵かきモード(web版では未実装) -----
  LineThicknessColor:  [[5.0,  1.0, 1.0, 1.0, 1.0]],
  PaletteScaleXY:      [[0.15, -2.5, 1.2]],

// ---- Plot モード ----- Ckk 1.23以降
  ArrowsName:   [''], 
  ArrowsSuffix:   [''], 
  ArrowsOn: [[1]],
  ArrowsInitDisplay: [[1]],
  LinesName:   [''], 
  LinesSuffix:   [''], 
  LinesOn: [[1]],
  LinesInitDisplay: [[1]],
  PointsName:   [''], 
  PointsSuffix:   [''], 
  PointsOn: [[1]],
  PointsInitDisplay: [[1]],
};


DOW.Config.prototype = {

  load: function( conf_files, loaded_handler, ignore_cache ) {

    if (conf_files.length == 0) {

      if (! this.has_error) loaded_handler();
      else {
        this.app.console.showError( DOW.MESSAGES.M1001 );
      }

    } else {

      var xhr = new XMLHttpRequest();
      var path = conf_files[0];
      var force_reload_trailer = ignore_cache ? ('?'+Date.now()) : '';

      var _this = this;

      xhr.onreadystatechange = function () {

        if ( xhr.readyState === xhr.DONE ) {
          // ロードの結果が出たらステータスをチェック
          if ( xhr.status !== 200 && xhr.status !== 0 ) {
            _this.app.console.addDetail( 'E0001', path+' (error state:'+xhr.status+')');
            if (_this.app.console.hasError) _this.has_error = true;
          } else if ( xhr.responseText == null || xhr.responseText.length == 0) {
            _this.app.console.addDetail('E0012', path+' (Could not read)');
            if (_this.app.console.hasError) _this.has_error = true;
          } else {
            // 内容を行毎に読む
            var lines = xhr.responseText.split(/[\n\r]+/);
            for (var i in lines) {
              // 行頭が#でなければ無視
              if (lines[i][0] != "#") continue;
              
              var words = lines[i].split(/\s+/);
              var key = words[0].slice(1);

              // パラメータ名が定義されているかチェック
              var param_spec = DOW.Config.PARAMETER_DEFINITION[key];
              if (param_spec == undefined) {
                _this.app.console.addDetail('E0013', '"#'+key+'" (line '+i+' in '+path+')');
                if (_this.app.console.hasError) _this.has_error = true;
                continue;
              }
              
              // パラメータの個数をチェック
              if (Array.isArray(param_spec[0]) && words.length < param_spec[0].length + 1) {
                _this.app.console.addDetail('E0014', lines[i]+' (line '+i+' in '+path+')' );
                if (_this.app.console.hasError) _this.has_error = true;
                continue;
              } else if (! Array.isArray(param_spec[0]) && words.length < 2) {
                _this.app.console.addDetail('E0014', lines[i]+' (line '+i+' in '+path+')' );
                if (_this.app.console.hasError) _this.has_error = true;
                continue;
              }
              
              var param = Array.isArray(param_spec[0]) ? words.slice(1, param_spec[0].length+1) : words[1];
              var line = words.slice(0, Array.isArray(param_spec[0]) ? param_spec[0].length+1 : 2).join(' ');
              
              // 数値パラメータなら文字列を数値に変換
              if (Array.isArray(param)) {
                for (var j in param_spec[0]) {
                  if (typeof param_spec[0][j] === 'number') {
                    param[j] = parseFloat(param[j]);
                    if (isNaN(param[j])) {
                      _this.app.console.addDetail('E0015', line + ' (line '+i+' of '+path+')' );
                      if (_this.app.console.hasError) _this.has_error = true;
                      continue;
                    }
                  }
                }
              } else {
                if (typeof param_spec[0] === 'number') {
                  param = parseFloat(param);
                  if (isNaN(param)) {
                    _this.app.console.addDetail('E0015', line + ' (line '+i+' of '+path+')' );
                    if (_this.app.console.hasError) _this.has_error = true;
                    continue;
                  }
                }
              }
              
              // パラメータの後処理が必要ならそれを施す
              if (param_spec.length > 1) {
                try {
                  param = param_spec[1](param);
                } catch (e) {
                  _this.app.console.addDetail('E0019', line + ' (line '+i+' of '+path+')' );
                  if (_this.app.console.hasError) _this.has_error = true;
                  continue;
                }
              }
              
              // ここまで失敗が無ければ設定値を上書きする
              _this[key] = param;
            }
          }

          // 残りの設定ファイルがあればそれを読み込む
          _this.load(conf_files.slice(1), loaded_handler, ignore_cache );

        }
      };
      
      xhr.open('GET', path+force_reload_trailer, true);
      xhr.setRequestHeader('Pragma', 'no-cache');
      xhr.setRequestHeader('Cache-Control', 'no-cache');
      DOW.log('load config: '+path+force_reload_trailer);
      xhr.send(null);
      
    }

  },

  debug_log: function() {
    for (var key in DOW.Config.PARAMETER_DEFINITION)
      DOW.log(key + ': ' + this[key]);
  },

};
/**
 * VideoLoader and ImageLoader based on THREE.ImageLoader
 *
 * VideoLoader は THREE.ImageLoaderを元に、ビデオ要素を生成するようにしたもの。
 * エラー表示で後から必要になるので、指定したパス名をしまう specified_path プロパティを追加。
 */

// VideoLoader

DOW.VideoLoader = function ( manager ) {

	this.cache = new THREE.Cache();
	this.manager = ( manager !== undefined ) ? manager : THREE.DefaultLoadingManager;

};

DOW.VideoLoader.prototype = {

	constructor: DOW.VideoLoader,

	load: function ( url, onLoad, onProgress, onError ) {

		var scope = this;

		var cached = scope.cache.get( url );

		if ( cached !== undefined ) {

			onLoad( cached );
			return;

		}

		var video = document.createElement( 'video' );

		if ( onLoad !== undefined ) {

			video.addEventListener( 'canplay', function ( event ) {

				scope.cache.add( url, this );

				onLoad( this );
				scope.manager.itemEnd( url );

        video.removeEventListener( 'canplay', arguments.callee, false );
			}, false );

		}

		if ( onProgress !== undefined ) {

			video.addEventListener( 'progress', function ( event ) {

				onProgress( event );

			}, false );

		}

		if ( onError !== undefined ) {

			video.addEventListener( 'error', function ( event ) {

				onError( event );

			}, false );

		}

		if ( this.crossOrigin !== undefined ) video.crossOrigin = this.crossOrigin;

		video.src = url;
    video.specified_path = url;

		scope.manager.itemStart( url );

		return video;

	},

	setCrossOrigin: function ( value ) {

		this.crossOrigin = value;

	}

}


// ImageLoader

DOW.ImageLoader = function ( manager ) {

	this.cache = new THREE.Cache();
	this.manager = ( manager !== undefined ) ? manager : THREE.DefaultLoadingManager;

};

DOW.ImageLoader.prototype = {

	constructor: DOW.ImageLoader,

	load: function ( url, onLoad, onProgress, onError ) {

		var scope = this;

		var cached = scope.cache.get( url );

		if ( cached !== undefined ) {

			onLoad( cached );
			return;

		}

		var image = document.createElement( 'img' );

		if ( onLoad !== undefined ) {

			image.addEventListener( 'load', function ( event ) {

				scope.cache.add( url, this );

				onLoad( this );
				scope.manager.itemEnd( url );

			}, false );

		}

		if ( onProgress !== undefined ) {

			image.addEventListener( 'progress', function ( event ) {

				onProgress( event );

			}, false );

		}

		if ( onError !== undefined ) {

			image.addEventListener( 'error', function ( event ) {

				onError( event );

			}, false );

		}

		if ( this.crossOrigin !== undefined ) image.crossOrigin = this.crossOrigin;

		image.src = url;
    image.specified_path = url;

		scope.manager.itemStart( url );

		return image;

	},

	setCrossOrigin: function ( value ) {

		this.crossOrigin = value;

	}

}
/***

    テクスチャーアニメーションとその制御

    TextureAnimator:
        Three.jsのテクスチャーをアニメーションさせるクラス。
        生成時の引数としてアニメーションの素材を渡す。
　　　　素材は、Imageの配列か、HTMLビデオ要素。

    AnimationController:
        複数のTextureAnimatorの再生を制御するクラス。
        プロパティとして以下を持つ
        speed: 再生速度（1は標準再生速度、負数は逆再生）
        loop: ループ再生するかどうか
        firstFramePause, lastFramePause: ループする場合の冒頭と最後のポーズ時間（秒）
　　　　※「標準再生速度」とは、ビデオ要素ならその1倍速、静止画連番なら5fps（ckkの標準速度）
        　複数の素材の長さが異なる場合は、最も長いものに合わせて個々の素材の速度が調整される。

 ***/

DOW.AnimationController = function( ) {
  this.type = 'AnimationController';

  // Open Properties
  this.loop = false;
  this.firstFramePause = 0;
  this.lastFramePause = 0;

  // Internal properties
  this._syncMaster = new DOW.AnimatorSyncMaster();
  this._sequence_player = undefined;
  // Status
  this._player_state = 'STOPED';
};

DOW.AnimationController.prototype = {

  get status()  {
    if (this._player_state == 'STOPED') return 0;
    return this._syncMaster.speed > 0 ? 1 : -1;
  },

  get speed()      { return this._syncMaster.speed; },
  set speed( val ) { this._syncMaster.speed = val; },

  set numberOfFrames( f ) {
    this._syncMaster.numberOfFrames = f;
  },

  add: function( anims ) {
    if ( Array.isArray( anims ) )
      anims.forEach( function( e, i, a ) { this._syncMaster.add_animator( e ); }, this );
    else
      this._syncMaster.add_animator( anim );
  },

  play: function() {
    if (this._player_state == 'STOPED') {
      this._player_state = 'START_PLAY';
      //DOW.log('STATE -> '+ this._player_state);
    }
  },

  pause: function() {
    if (this._player_state != 'STOPED') {
      this._syncMaster.pause();
      this._player_state = 'STOPED';
      //DOW.log('STATE -> '+ this._player_state);
    }
  },

  seek: function( position ) {
    this.pause();
    this._syncMaster.seek( position );
  },

  step: function( s ) {
    var f = this._syncMaster.frame;
    this._syncMaster.frame = f + s;
    DOW.log("step: "+this._syncMaster.frame);
  },

  // 毎フレーム呼ばれる
  update: function( deltaTime ) {
    
    if (this._player_state === 'STOPED') {
    }

    if (this._player_state === 'START_PLAY') {
      if (this._speed > 0 && this._syncMaster.currentPosition == 1) this._syncMaster.seek(0);
      if (this._speed < 0 && this._syncMaster.currentPosition == 0) this._syncMaster.seek(1);
      this._syncMaster.play();
      this._player_state = 'PLAYING';
      //DOW.log('STATE -> '+ this._player_state + '(' + this._syncMaster.currentPosition + ')');
    }

    if (this._player_state === 'PLAYING') {
      this._syncMaster.update( deltaTime );
      if (! this._syncMaster.playing) {
        if (this.loop) {
          this._pause_timer = this._syncMaster.speed > 0 ? this.lastFramePause : this.firstFramePause;
          this._player_state = 'POST_PAUSE';
        } else {
          this._syncMaster.pause();
          this._player_state = 'STOPED';
        }
        //DOW.log('STATE -> '+ this._player_state + '(' + this._syncMaster.currentPosition + ')');
      }
    }

    if (this._player_state === 'POST_PAUSE') {
      if (this._pause_timer > 0) {
        this._pause_timer -= deltaTime;
      } else {
        this._syncMaster.seek( this._syncMaster.speed > 0 ? 0 : 1 );
        this._syncMaster.update( deltaTime );
        this._pause_timer = this._syncMaster.speed > 0 ? this.firstFramePause : this.lastFramePause;
        //this._player_state = 'PRE_PAUSE';
        this._player_state = 'WAIT_SEEK';
        //DOW.log('STATE -> '+ this._player_state + '(' + this._syncMaster.currentPosition + ')');
      }
    }

    if (this._player_state === 'WAIT_SEEK') {
      if (this._syncMaster.playable) {
        this._player_state = 'PRE_PAUSE';
        //DOW.log('STATE -> '+ this._player_state + '(' + this._syncMaster.currentPosition + ')');
      }
    }

    if (this._player_state === 'PRE_PAUSE') {
      if (this._pause_timer > 0) {
        this._pause_timer -= deltaTime;
      } else {
        this._syncMaster.play();
        this._player_state = 'PLAYING';
        //DOW.log('STATE -> '+ this._player_state + '(' + this._syncMaster.currentPosition + ')');
      }
    }

  },

};

DOW.TextureAnimator = function( resource ) {
  this.type = 'TextureAnimator';

  // Resource
  if (resource instanceof HTMLVideoElement) {
    this._texture = new THREE.VideoTexture(resource);
    this._duration = resource.duration;
    this.video = resource;
  } else if ( Array.isArray(resource) ) {
    this._time = 0;
    this._current_frame = 0;
    this._texture = new THREE.Texture(resource[0]);
    this._duration = resource.length / DOW.CKK_STD_FPS;
    this.sequence = resource;
  }
  this._texture.needsUpdate = true;

  this._playing = false;
};

DOW.TextureAnimator.prototype = {

  constructor: DOW.TextureAnimator,

  get isVideo() { return this.hasOwnProperty('video'); },

  get duration() { return this._duration; },

  get texture() {  return this._texture; },

  get aspect() {
    return this.isVideo ? this.video.videoWidth / this.video.videoHeight : this.sequence[0].width / this.sequence[0].height;
  },

  seek: function( position ) {
    if (this.isVideo) {
      this.video.currentTime = position * this.video.duration;
      //DOW.log("seekable: "+this.video.seekable.start(0)+" - "+this.video.seekable.end(0)+" currentTime:"+(position * this.video.duration)+"->"+this.video.currentTime);
    } else {
      var index = Math.floor(position * this.sequence.length);
      if (index < 0) index = 0;
      if (index > this.sequence.length - 1) index = this.sequence.length - 1;
      if (this._current_frame != index) {
        this._current_frame = index;
        this._texture.image = this.sequence[index];
        this._texture.needsUpdate = true;
      }
    }
  },

};

DOW.AnimatorSyncMaster = function AnimatorSyncMaster() {
  this.type = 'AnimatorSyncMaster';

  this.videos = [];
  this.sequences = [];
  this.masterVideo = undefined;

  this._duration = 1 / DOW.CKK_STD_FPS;
  this.numberOfFrames = 0;
  this._position = 0;
  this._speed = 1;
  this._playing = false;
};

DOW.AnimatorSyncMaster.prototype = {

  get currentTime() {    return this._position * this._duration;  },

  get currentPosition() {    return this._position;  },

  get playable() {
    return this.videos.every( function( e, i, a ) { return e.video.readyState == HTMLMediaElement.HAVE_ENOUGH_DATA && ! e.video.seeking; } );
  },

  get playing() {
    return this._playing;
  },

  get duration() { return this._duration; },
  set duration( val ) {
    this._duration = val;
    this.videos.forEach( function( e, i, a ) { e.video.playbackRate = this._speed * e.video.duration / this._duration;}, this );
  },

  get frame() {
    return this._position == 1 ? this.numberOfFrames - 1 : Math.floor(this._position * this.numberOfFrames);
  },
  set frame( f ) {
    var pos = (f + 0.5) / this.numberOfFrames;
    if (f <= 0) pos = 0;
    else if (f >= this.numberOfFrames - 1) pos = 1;
    this.seek( pos );
    DOW.log("frame <- "+pos);
  },

  get speed() {    return this._speed;  },
  set speed( val ) {
    this._speed = val;
    this.videos.forEach( function( e, i, a ) { e.video.playbackRate = this._speed * e.video.duration / this._duration;}, this );
  },

  add_animator: function( anim ) {
    if ( anim.isVideo ) {
      this.videos.push( anim );
      if (this._duration < anim.video.duration) {
        this._duration = anim.video.duration;
        this.masterVideo = anim;
        this.videos.forEach( function( e, i, a ) { e.video.playbackRate = this._speed * e.video.duration / this._duration;}, this );
      }
    } else {
      this.sequences.push( anim );
      if (this.numberOfFrames < anim.sequence.length) {
        this.numberOfFrames = anim.sequence.length;
      }
      if (this._duration < anim._duration) {
        this._duration = anim.duration;
        this.videos.forEach( function( e, i, a ) { e.video.playbackRate = this._speed * e.video.duration / this._duration;}, this );
      }
    }
  },
  
  play: function() {
    this._playing = true;
    this.videos.forEach( function( e, i, a ) { e.video.play(); } );
  },

  pause: function() {
    this._playing = false;
    this.videos.forEach( function( e, i, a ) { e.video.pause(); } );
  },

  seek: function( pos ) {
    this._position = pos;
    this.videos.forEach( function( e, i, a ) { e.seek( pos ); });
    this.sequences.forEach( function( e, i, a ) { e.seek( pos ); });
  },

  update: function( deltaTime ) {
    if (! this._playing) return;

    if (this.masterVideo != undefined) {
      this._position = this.masterVideo.video.currentTime / this.masterVideo.video.duration;
    } else {
      this._position += deltaTime * this._speed / this._duration;
    }

    if (this._position >= 1 && this._speed > 0) {
      this._playing = false;
      this._position = 1;
    } else if (this._position <= 0 && this._speed < 0) {
      this._playing = false;
      this._position = 0;
    }

    this.sequences.forEach( function( e, i, a ) { e.seek( this._position ); }, this );
  },

};


DOW.Trackball = function( app ) {
  this.type = 'Tracker';

  this.app = app;
  // GUIエレメントにイベントハンドラーを仕掛ける（全面を覆う最上位のレイヤなので）
  this.element = app.gui.domElement;
  this.deltaRotation = new THREE.Quaternion();
  this._center = new THREE.Vector2();
  this._radius = 1.0;

  var _this = this;

  var lastVector = new THREE.Vector3();
  var currentVector = new THREE.Vector3();
  var startLoc = new THREE.Vector2();

  /* 画面座標を地球表面を指すベクトルに変換する */
  function xy2dagik2d( x, y, vec2d ) {
    // Dagik座標系に変換
    var ih = 1 / _this.element.clientHeight;
    vec2d.x = DOW.WTOP * (2*x - _this.element.clientWidth) * ih;
    vec2d.y = DOW.WTOP * (1 - y * 2 * ih);
  }
  function xy2earth3d( x, y, earth3d, center2d ) {
    // Dagik座標系に変換
    var ih = 1 / _this.element.clientHeight;
    var x2 = DOW.WTOP * (2*x - _this.element.clientWidth) * ih;
    var y2 = DOW.WTOP * (1 - y * 2 * ih);

    // 地球座標系に変換
    x = (x2 - center2d.x) / _this._radius;
    y = (y2 - center2d.y) / _this._radius;
    var rr = x*x + y*y;
    var r = Math.sqrt(rr);

    if (rr > 1) {
      earth3d.set(x/r, y/r, 0);
    } else {
      earth3d.set(x, y, Math.sqrt(1-rr));
    }

    return r;
  }

  this.update = function() {
    this.deltaRotation.setFromUnitVectors( lastVector, currentVector );
    lastVector.copy(currentVector);
  }

  this.start_track = function( x, y ) {
    xy2dagik2d(x, y, startLoc);
    var r = xy2earth3d(x, y, currentVector, DOW.CKK_ROTATION_POLICY ? startLoc : _this._center);
    lastVector.copy(currentVector);
    _this.deltaRotation.set(0, 0, 0, 1);

    return r < 1.1;
  }


  this.track = function( x, y ) {
    xy2earth3d(x, y, currentVector, DOW.CKK_ROTATION_POLICY ? startLoc : _this._center);
  }

  function mouseMove( ev ) {
    ev.preventDefault();
    ev.stopPropagation();
    _this.track( ev.clientX, ev.clientY );
  }

  function mouseUp( ev ) {
    ev.preventDefault();
    ev.stopPropagation();
    _this.element.removeEventListener( 'mousemove', mouseMove );
    _this.element.removeEventListener( 'mouseup', mouseUp );
  }

  function mouseDown( ev ) {
    if (_this.start_track( ev.clientX, ev.clientY )) {
      ev.preventDefault();
      ev.stopPropagation();
      _this.element.addEventListener( 'mousemove', mouseMove, false );
      _this.element.addEventListener( 'mouseup', mouseUp, false );
    }
  }

  var start_touch_id = -1;
  function touchMove( ev ) {
    ev.preventDefault();
    ev.stopPropagation();
    var t = undefined;
    for (var i in ev.changedTouches) {
      if (ev.changedTouches[i].identifier == start_touch_id) {
        t = ev.changedTouches[i];
        break;
      }
    }
    if (t != undefined) {
      _this.track( t.clientX, t.clientY );
    }
  }

  function touchEnd( ev ) {
    ev.preventDefault();
    ev.stopPropagation();
    start_touch_id = -1;
    _this.element.removeEventListener( 'touchmove', touchMove );
    _this.element.removeEventListener( 'touchend', touchEnd );
    _this.element.removeEventListener( 'touchcancel', touchEnd );
  }

  function touchStart( ev ) {
    var t = ev.targetTouches[0];
    // タッチ中ではなく、ボタン上でもなく、地球の上（よりちょっと外側までオーケー）ならタッチ処理を開始する。
    if (start_touch_id < 0 && ev.target == _this.element  && _this.start_track( t.clientX, t.clientY )) {
      ev.preventDefault();
      ev.stopPropagation();

      start_touch_id = t.identifier;
      _this.element.addEventListener( 'touchmove', touchMove, false );
      _this.element.addEventListener( 'touchend', touchEnd, false );
      _this.element.addEventListener( 'touchcancel', touchEnd, false );
    }
    
  }

	this.element.addEventListener( 'mousedown', mouseDown, false );

  /*
	this.domElement.addEventListener( 'mousewheel', mousewheel, false );
	this.domElement.addEventListener( 'DOMMouseScroll', mousewheel, false ); // firefox
  */

	this.element.addEventListener( 'touchstart', touchStart, false );

  /*
	this.domElement.addEventListener( 'touchend', touchEnd, false );
	this.domElement.addEventListener( 'touchmove', touchMove, false );
	window.addEventListener( 'keydown', keydown, false );
	window.addEventListener( 'keyup', keyup, false );
  */

};

DOW.Trackball.prototype = {
  
  set center( value ) {
    this._center.set( value[0], value[1] );
  },

  get radius() {
    return this._radius;
  },

  set radius( value ) {
    this._radius = value;
  },

};


DOW.GUI = function( app ) {
  this.type = 'GUI';

  this.buttons = {};

  this.domElement = document.createElement('div');
  this.domElement.id = 'dow_ui';
  this.domElement.style.position = 'absolute';
  this.domElement.style.top = 0;
  this.domElement.style.left = 0;
  this.domElement.style.margin = 0;
  this.domElement.style.width = '100%';
  this.domElement.style.height = '100%';
  
  for (var i in DOW.ICONS) {
    var desc = DOW.ICONS[i];
    if (app.config[desc[0]] === undefined) continue;

    var config_name = desc[0];
    var conf = app.config[config_name];
    if (conf[0] == 0) continue;

    var button = document.createElement('img');
    button.className = 'button';
    // ボタン固有の値
    button.function_name = desc[1];
    button.icon1 = app.icondir + '/' + desc[2];
    button.icon2 = desc.length > 3 ? app.icondir + '/' + desc[3] : undefined;
    button.conf = conf;

    this.domElement.appendChild(button);
    button.src = button.icon1;
    button.onclick = function() {
      app['command_'+this.function_name]();
    };

    this.buttons[button.function_name] = button;
  }

};

DOW.GUI.prototype = {

  layout: function() {
    var ratio = this.domElement.clientHeight / 4;
    var center = this.domElement.clientWidth / 2;
    for (var key in this.buttons) {
      var button = this.buttons[key];
      var left = button.conf[1] * ratio + center;
      var top = (DOW.WTOP - button.conf[2]) * ratio;
      button.style.position = 'absolute';
      button.style.top = top+'px';
      button.style.left = left+'px';
      var sz = (button.conf[0]*ratio)+'px';
      button.style.width = sz;
      button.style.height = sz;
    }
  },



};

DOW.Earth = function( layers ) {
  this.type = 'Earth';

  this.layers = layers;

  this.textures = layers.map( function( e, i, a ) {
      var texture = e.texture;
      //texture.minFilter = THREE.NearestFilter;
      // 意外といいがピクセルが目立つ。
      //texture.minFilter = THREE.NearestMipMapNearestFilter;
      // 同心円と放射状の区切れがハッキリ見えすぎ。
      //texture.minFilter = THREE.NearestMipMapLinearFilter;
      // 同心円と放射状の区切れがハッキリ見えすぎ。
      texture.minFilter = THREE.LinearFilter;
      // 意外といい。これにAnisotropicを組み合わせるといいかも。
      //texture.minFilter = THREE.LinearMipMapNearestFilter;
      // 極への集中がハッキリ
      //texture.minFilter = THREE.LinearMipMapLinearFilter;
      // 極への集中がハッキリ
      //texture.anisotropy = 4;
      // Anisotoropicは入れてもダメ。そもそもMipMapが悪い。
      texture.generateMipmaps = false;
      return texture;
    });
  this.mesh = this.setup_mesh( this.textures );
};

DOW.Earth.prototype = {

  get scale() {
    return this.mesh.scale.x;
  },
  set scale ( value ) {
    this.mesh.scale.set(value, value, value);
  },

  get position() {
    return [this.mesh.position.x, this.mesh.position.y];
  },
  set position ( xy ) {
    this.mesh.position.set(xy[0], xy[1], 0);
  },

  get rotation() {
    return this.mesh.rotation;
  },
  set rotation( q ) {
    this.mesh.rotation.copy( q );
  },

  setFrontLonLat: function( lon, lat ) {
    var rad = Math.PI / 180.0;
    this.mesh.rotation.set(lat*rad, -(lon+90)*rad, 0, 'XYZ');
  },

  localRotate: function( quaternion ) {
    this.mesh.quaternion.multiply(quaternion);
  },

  rotate: function( quaternion ) {
    this.mesh.quaternion.multiplyQuaternions(quaternion, this.mesh.quaternion);
  },

  northUp: function() {
  },

  changeLayer: function() {
  },

  setup_mesh: function( textures ) {
    var material = new THREE.MeshBasicMaterial({ map: textures[0] });
    var mesh = new THREE.Mesh(
      new THREE.SphereGeometry(1.0, 64, 64),
      material
      );
    return mesh;
  },

}


DOW.Screen = function( layers ) {
  this.type = 'Screen';

  this.layers = layers;

  this.textures = layers.map( function( e, i, a ) { return e.texture; });
  this.aspect = layers[0].aspect || 1;

  this.mesh = this.setup_mesh( this.textures );

  this.top_left = [0, 0];
};

DOW.Screen.prototype = {

  get scale() {
    return this.mesh.scale.x;
  },
  set scale ( value ) {
    var half_h = value * 0.5;
    var half_w = half_h * this.aspect;
    this.mesh.scale.set(value, value, value);
    this.mesh.position.set(this.top_left[0]+half_w, this.top_left[1]-half_h, 0);
  },

  get position() {
    return this.top_left;
  },
  set position ( xy ) {
    var half_h = this.mesh.scale.y * 0.5;
    var half_w = half_h * this.aspect;
    this.mesh.position.set(xy[0]+half_w, xy[1]-half_h, 0);
    this.top_left = xy;
  },

  get visible() {
    return this.mesh.visible;
  },
  set visible( value ) {
    this.mesh.visible = value;
  },
  
  changeLayer: function() {
  },

  setup_mesh: function( textures ) {
    var material = new THREE.MeshBasicMaterial({ map: textures[0] });
    var mesh = new THREE.Mesh(
      new THREE.PlaneBufferGeometry(this.aspect, 1, 1, 1),
      material
      );
    return mesh;
  },

}

/***

DOW.Application: Web版Dagik_Earthのプログラム本体

使い方：

var app = new DOW.Application( element_id, { パラメータ } );
app.Run();

パラメータ：
  element_id: Dagik Earth を埋め込むHTML要素のID名
  configs: 設定ファイルの配列（デフォルトは['data/conf/init_conf.txt', 'data/conf/conf.txt', 'data/conf/dow_conf.txt']）
  icondir: インタフェース用アイコン(png)のディレクトリ（デフォルトは js/icons）
***/


DOW.Application = function( element_id, params ) {

  this.type = 'Application';

  this.configs = params.hasOwnProperty('configs') ? params.configs : ['data/conf/init_conf.txt', 'data/conf/conf.txt', 'data/conf/dow_conf.txt'];
  this.icondir = params.hasOwnProperty('icondir') ? params.icondir : 'js/icons';

  this.domElement = document.getElementById(element_id);
  this.domElement.className = 'dow_main';
  this.domElement.style.position = 'relative';

  // 設定ファイルを読み込み値を管理する
  this.config = new DOW.Config( this );
  // 処理中の表示やエラー表示をする画面を用意
  this.console = new DOW.Console( this, this.domElement );
  this.domElement.appendChild(this.console.domElement);
  this.console.hide();

  this.renderer;
  this.scene;
  this.camera;
  // GL描画要素
  this.earth1;
  this.screen1;
  this.earth2;
  this.screen2;
  // 操作要素
  this.gui;
  this.trackball;

};


DOW.Application.prototype = {

  Run: function() {

    // WebGL環境があるか確認する
    if (! Detector.webgl) {
      this.console.showError( DOW.MESSAGES.E0001 );
      return;
    }
    
    // 読み込み中画面を表示する
    this.console.showProgress( DOW.MESSAGES.M0001 );

    // 設定ファイルを読み込む（読み終わったら load_resources を呼ぶ）
    var app = this;
    this.config.load (
      // config files
      this.configs,
      // callback function
      function() { app.load_resources(); },
      // ignore cache
      true
      );
    
  },

  /*
    load_resources: 設定ファイルの読み込みが終わると呼び出されるハンドラ
    （画像ファイル等を読み込んで、読み終わったら start_rendering を呼ぶ）
  */
  load_resources: function() {
    var app = this;

    // E0018: Web版では、連番ファイルの末尾の番号が明示されていない（init_conf.txtに書かれた9999のまま）場合エラーとする。
    if (this.config.TextureEnd == 9999 && DOW.IGNORE_ERRORS.indexOf('E0018') < 0) {
      this.console.showError( DOW.MESSAGES.M1001 );
      this.console.addDetail( 'E0018', '' );
      return;
    }

    this.console.showProgress( DOW.MESSAGES.M0002 );

    this.resources = {};
    var resources = [];
    // 素材名をAlt形式に整形
    var image_range = '['+this.config.TextureStart+'-'+(this.config.TextureEnd-1)+']';
    if (this.config.AltTextureName[0] == '') {
      if (this.config.VideoTextureName) {
        this.config.AltTextureName = [this.config.VideoTextureName, 'video'];
      } else {
        this.config.AltTextureName = [this.config.TextureName+image_range+'.'+this.config.TextureSuffix, 'images'];
      }
    }
    if (this.config.AltSecondTextureName[0] == '') {
      if (this.config.SecondVideoTextureName) {
        this.config.AltSecondTextureName = [this.config.SecondVideoTextureName, 'video'];
      } else {
        this.config.AltSecondTextureName = [this.config.SecondTextureName+image_range+'.'+this.config.SecondTextureSuffix, 'images'];
      }
    }
    if (this.config.AltTextureName2[0] == '') {
      if (this.config.VideoTextureName2) {
        this.config.AltTextureName2 = [this.config.VideoTextureName2, 'video'];
      } else {
        this.config.AltTextureName2 = [this.config.TextureName2+image_range+'.'+this.config.TextureSuffix2, 'images'];
      }
    }
    if (this.config.AltSecondTextureName2[0] == '') {
      if (this.config.SecondVideoTextureName2) {
        this.config.AltSecondTextureName2 = [this.config.SecondVideoTextureName2, 'video'];
      } else {
        this.config.AltSecondTextureName2 = [this.config.SecondTextureName2+image_range+'.'+this.config.SecondTextureSuffix2, 'images'];
      }
    }
    if (this.config.AltScreenName[0] == '') {
      if (this.config.VideoScreenName) {
        this.config.AltScreenName = [this.config.VideoScreenName, 'video'];
      } else {
        this.config.AltScreenName = [this.config.ScreenName+image_range+'.'+this.config.ScreenSuffix, 'images'];
      }
    }
    if (this.config.AltSecondScreenName[0] == '') {
      if (this.config.SecondVideoScreenName) {
        this.config.AltSecondScreenName = [this.config.SecondVideoScreenName, 'video'];
      } else {
        this.config.AltSecondScreenName = [this.config.SecondScreenName+image_range+'.'+this.config.SecondScreenSuffix, 'images'];
      }
    }
    if (this.config.AltScreenName2[0] == '') {
      if (this.config.VideoScreenName2) {
        this.config.AltScreenName2 = [this.config.VideoScreenName2, 'video'];
      } else {
        this.config.AltScreenName2 = [this.config.ScreenName2+image_range+'.'+this.config.ScreenSuffix2, 'images'];
      }
    }
    if (this.config.AltSecondScreenName2[0] == '') {
      if (this.config.SecondVideoScreenName2) {
        this.config.AltSecondScreenName2 = [this.config.SecondVideoScreenName2, 'video'];
      } else {
        this.config.AltSecondScreenName2 = [this.config.SecondScreenName2+image_range+'.'+this.config.SecondScreenSuffix2, 'images'];
      }
    }

    // マッピング画像の読み込みを開始
    this.resources.earth1_layer1 = this.load_layer(this.config.AltTextureName, this.config.AltScreenName);
    resources.push(this.resources.earth1_layer1);
    // 第2レイヤーがあればその素材も読む
    if (this.config.NumberOfTextures == 2) {
      this.resources.earth1_layer2 = this.load_layer(this.config.AltSecondTextureName, this.config.AltSecondScreenName, false); // 読み込みエラーは許さない
      resources.push(this.resources.earth1_layer2);
    }
    // 二つ目の地球が別の画像を使う場合、それを読み込む
    if (this.config.NumberOfEarth == 2 && this.config.TextureOfEarth2) {
      this.resources.earth2_layer1 = this.load_layer(this.config.AltTextureName2, this.config.AltScreenName2);
      resources.push(this.resources.earth2_layer1);
      if (this.config.NumberOfTextures == 2) {
        this.resources.earth2_layer2 = this.load_layer(this.config.AltSecondTextureName2, this.config.AltSecondScreenName2, false); // 読み込みエラーは許さない
        resources.push(this.resources.earth2_layer2);
      }
    }
     
    // 画像の読み込み完了を待って、完了したらstart_renderingを呼ぶ
    var isDone = function(e, i, a) { return e.done; };
    var progress = function(v, e, i, a) {
      v[0] += e.load_count;
      v[1] += e.error_count;
      v[2] += e.total_count;
      return v;
    };
    var load_check = function() {
      var progress_count = resources.reduce( progress, [0, 0, 0] );
      app.console.showProgress(DOW.MESSAGES.M0002+': '+progress_count[0]+' / '+progress_count[2]+' ('+progress_count[1]+' errors)');

      if ( resources.every(isDone) ) {
        if (app.console.hasError) {
          app.console.showError( DOW.MESSAGES.M1001 );
        } else {
          app.start_rendering();
        }
      } else {
        window.setTimeout(load_check, 200);
      }
    };
    load_check();
  },

  load_layer: function( texture_name, screen_name, allow_errors) {
    DOW.log('load_layer: '+texture_name[0] +', '+ screen_name[0]);
    var res = { done: false, load_count: 0, error_count: 0, total_count: 0, map: undefined, sceren: undefined };
    var onLoad = function(e) {
      res.load_count++;
      if ( res.load_count + res.error_count == res.total_count ) res.done = true;
    }
    var app = this;
    var onError = function(e) {
      res.error_count++;
      if ( res.load_count + res.error_count == res.total_count ) res.done = true;
      if (allow_errors) {
        res.map = undefined;
        res.screen = undefined;
      } else {
        app.console.addDetail( 'E0101', e.target.specified_path);
      }
    };
    // マッピング画像の読み込みを開始
    if (texture_name[1] == 'image') {
      res.map = this.load_image( texture_name[0], onLoad, onError);
      res.total_count ++;
    } else if (texture_name[1] == 'images') {
      res.map = this.load_images( texture_name[0], onLoad, onError);
      res.total_count += res.map.length;
    } else if (texture_name[1] == 'video') {
      res.map = this.load_video(texture_name[0], onLoad, onError);
      res.total_count ++;
    }

    // キャプション画像の読み込みを開始
    if (screen_name[1] == 'image') {
      res.screen = this.load_image( screen_name[0], onLoad, onError);
      res.total_count ++;
    } else if (screen_name[1] == 'images') {
      res.screen = this.load_images( screen_name[0], onLoad, onError);
      res.total_count += res.screen.length;
    } else if (screen_name[1] == 'video') {
      res.screen = this.load_video( screen_name[0], onLoad, onError);
      res.total_count ++;
    }

    res.done = res.total_count == res.load_count;
    return res;
  },

  load_image: function(file, onLoad, onError) {
    var loader = new DOW.ImageLoader();
    var result = [];
    result.push(loader.load(file+'?'+Date.now(), onLoad, undefined, onError));
    return result;
  },

  load_video: function(file, onLoad, onError) {
    var loader = new DOW.VideoLoader();
    return loader.load(file, onLoad, undefined, onError);
  },

  load_images: function(file, onLoad, onError) {
    var loader = new DOW.ImageLoader();
    var result = [];
    var pattern = /^(.+)\[(\d+)-(\d+)\](.+)$/;
    var match = pattern.exec(file);
    if (match == null) {
      this.console.addDetail( 'E0102', file );
    } else {
      var base = match[1];
      var start = parseInt(match[2]);
      var end = parseInt(match[3])+1;
      var tail = match[4];
      for (var i=start; i<end; i++) {
        result.push(loader.load(base+i+tail, onLoad, undefined, onError));
      }
    }
    return result;
  },


  /*
    start_rendering: リソースの読み込みが終わると呼び出されるハンドラ
  */
  start_rendering: function() {

    var app = this;

    this.console.showProgress('読み込みが完了しました');
    this.console.progressing = false;

    // WebGL環境をセットアップしてMessageViewの後ろに置く
    this.renderer = new THREE.WebGLRenderer();
    this.domElement.insertBefore(this.renderer.domElement, this.console.domElement);
    this.renderer.setSize(this.domElement.clientWidth, this.domElement.clientHeight);
    this.renderer.setClearColor(new THREE.Color(0, 0, 0), 1);

    // カメラをセットアップする
    var aspect = this.domElement.clientWidth / this.domElement.clientHeight;
    if (this.config.Perspective) {
      var fovy = Math.atan(DOW.WTOP / this.config.EyePosition) * 2 * 180/Math.PI;
      this.camera = new THREE.PerspectiveCamera(fovy, aspect, 0.01, 1000);
    } else {
      this.camera = new THREE.OrthographicCamera(-DOW.WTOP * aspect, DOW.WTOP * aspect, DOW.WTOP, -DOW.WTOP, 0.01, 1000);
    }
    this.camera.position.z = this.config.EyePosition;
    this.camera.lookAt( new THREE.Vector3(0, 0, 0) );

    // アニメーションコントローラーを準備する。
    this.animation_speed = this.config.AnimationSpeed;
    this.animationController = new DOW.AnimationController();
    this.animationController.speed = this.speedFromCKK(this.animation_speed);
    this.animationController.loop = this.config.Repeat;
    this.animationController.firstFramePause = this.config.StopAt1stMap;
    this.animationController.lastFramePause = this.config.StopAtFinalMap;
    if (this.config.NumberOfVideoFrames > 0) this.animationController.numberOfFrames = this.config.NumberOfVideoFrames;

    // 地球を作る
    if (this.resources.earth1_layer1.map !== undefined) {
      // 地球
      var layers = [ new DOW.TextureAnimator( this.resources.earth1_layer1.map ) ];
      if (this.resources.earth1_layer2 !== undefined) layers.push( new DOW.TextureAnimator( this.resources.earth1_layer2.map ) );
      this.animationController.add( layers );
      var earth = new DOW.Earth( layers );
      earth.scale = this.config.Scale;
      earth.position = DOW.CKK_EARTHXY_POLICY ? [this.config.EarthXY[0]*earth.scale, this.config.EarthXY[1]*earth.scale] : this.config.EarthXY;
      earth.setFrontLonLat(this.config.Longitude, this.config.Latitude);
      this.earth1 = earth;
    }
    if (this.resources.earth1_layer1.screen !== undefined) {
      // キャプション
      layers = [ new DOW.TextureAnimator( this.resources.earth1_layer1.screen ) ];
      if (this.resources.earth1_layer2 !== undefined) layers.push( new DOW.TextureAnimator( this.resources.earth1_layer2.screen ) );
      this.animationController.add( layers );
      var screen = new DOW.Screen( layers );
      screen.scale = this.config.ScreenScaleXY[0];
      screen.position = [this.config.ScreenScaleXY[1], this.config.ScreenScaleXY[2]];
      screen.visible = this.config.ScreenOn;
      this.screen1 = screen;
    }

    // シーンにまとめる
    this.scene = new THREE.Scene();
    if (this.earth1) this.scene.add(this.earth1.mesh);
    if (this.screen1) this.scene.add(this.screen1.mesh);
    if (this.earth2) this.scene.add(this.earth2.mesh);
    if (this.screen2) this.scene.add(this.screen2.mesh);
    this.scene.add(new THREE.AmbientLight(0xffffff));
    // 要素を画面内に配置する（リサイズ時にも呼ばれるのでハンドラとして作成）
    this.layout();

    // GUIを作ってConsoleの後ろに置く
    this.gui = new DOW.GUI( this );
    this.domElement.insertBefore(this.gui.domElement, this.console.domElement);
    this.gui.layout();

    // マウス入力を回転に変えるトラックボールコントロール
    this.trackball = new DOW.Trackball( this );
    this.trackball.radius = this.earth1.scale;
    this.trackball.center = this.earth1.position;
    
    // イベントハンドラを登録する
    document.addEventListener('keydown', function( ev ) { app.key_event( ev ); });
    document.addEventListener('keypress', function( ev ) { app.key_event( ev ); });
    // リサイズイベントハンドラ（連続するリサイズイベントを250msecのあいだ無効化する）
    var resize_call_count = 0;
    window.addEventListener( 'resize', function( ev ) {
        ++resize_call_count;
        window.setTimeout( function() {
            if (--resize_call_count > 0) return;
            app.layout();
            app.gui.layout();
          }, 250);
      } );

    //// 動作の初期状態を設定
    // テクスチャーアニメーション
    if (this.config.Animation == 1)
      this.command_forward_play();
    else if (this.config.Animation == -1)
      this.command_reverse_play();

    // 自転動作
    this.spin_status = this.config.Spin;
    this.spin_speed = this.config.SpinSpeed;
    // 表示レイヤ
    this.layer_status = this.config.SecondTexture_at_startup;
    
    // レンダリングをまわす。
    var lastTime = Date.now() * 0.001;
    var q = new THREE.Quaternion();
    var update_frame = function() {

      var timeDeltaSec = Date.now() * 0.001 - lastTime;
      lastTime += timeDeltaSec;

      // マウスの回転操作を処理
      app.trackball.update();
      if (! app.config.NoMouseDragRotation) {
        if (app.earth1 !== undefined) {
          app.earth1.rotate(app.trackball.deltaRotation);
        }
        if (app.earth2 !== undefined) {
          if (app.config.EarthRotationSynch)
            app.earth2.rotate(app.trackball.deltaRotation);
        }
      }

      // 自転:#SpinIntervalSecondあたりにjiten_kankaku(0.3度)*jitensokudo(設定値)/100
      // つまり、設定値は1秒間の回転角の1/30
      if (app.spin_status) {
        var angle = 0.003 * app.spin_speed * (timeDeltaSec/app.config.SpinIntervalSecond);
        q.setFromAxisAngle(new THREE.Vector3(0, 1, 0), angle * Math.PI/180);
        if (app.earth1 !== undefined) app.earth1.localRotate(q);
        if (app.earth2 !== undefined) app.earth2.localRotate(q);
      }

      // アニメーション
      app.animationController.update( timeDeltaSec );

      // GUIの表示更新
      var fwd_btn = app.gui.buttons['forward_play'];
      if (fwd_btn) fwd_btn.src = app.animationController.status != 1 ? fwd_btn.icon1 : fwd_btn.icon2;
      var rev_btn = app.gui.buttons['reverse_play'];
      if (rev_btn) rev_btn.src = app.animationController.status != -1 ? rev_btn.icon1 : rev_btn.icon2;
      
      // 次のフレームのレンダリング呼び出しを予約
      requestAnimationFrame(update_frame);
      
      // レンダリング自体はthree.jsにお任せ
      app.renderer.render(app.scene, app.camera);
    };

    this.console.hide();
    update_frame();    

  },

  layout: function() {
    // 領域のサイズを取得
    var width = this.domElement.clientWidth;
    var height = this.domElement.clientHeight;
    // Rendererの領域を更新
    this.renderer.setSize(width, height);
    // Cameraのアスペクト比を更新
    var aspect = width / height;
    if (this.config.Perspective) {
      this.camera.aspect = aspect;
    } else {
      this.camera.right = DOW.WTOP * aspect;
      this.camera.left = - DOW.WTOP * aspect;
    }
    this.camera.updateProjectionMatrix();
  },

  // キーイベントハンドラ
  key_event: function( ev ) {
    var key;
    if (ev.type == 'keydown') {
      switch ( ev.keyCode ) {
        // left:37, up:38, right:39, down:40, esc:27, enter:13
      case 13: key = 'ENTER'; break;
      case 27: key = 'ESCAPE'; break;
      case 37: key = 'LEFT_ARROW'; break;
      case 38: key = 'UP_ARROW'; break;
      case 39: key = 'RIGHT_ARROW'; break;
      case 40: key = 'DOWN_ARROW'; break;
      }
    } else if (ev.type == 'keypress') {
      key = String.fromCharCode( ev.charCode );
    }
    if (key !== undefined) {
      var command = DOW.KEY_ACTION[key];
      if (command !== undefined) this['command_'+command]();
    }
  },


  /********* コマンド *********/
  command_initialize: function() {
    DOW.log("command_initialize");
    window.location.reload();
  },

  command_fullscreen: function() {
    DOW.log("command_fullscreen");
    var fullscreen_element;
    if (document.fullscreenElement !== undefined) fullscreen_element = document.fullscreenElement;
    else if (document.webkitFullscreenElement !== undefined) fullscreen_element = document.webkitFullscreenElement;
    else if (document.mozFullScreenElement !== undefined) fullscreen_element = document.mozFullScreenElement;
    else if (document.msFullscreenElement !== undefined) fullscreen_element = document.msFullscreenElement;
    else return;
    
    if (fullscreen_element == null) {
      if (this.domElement.requestFullscreen) this.domElement.requestFullscreen();
      else if (this.domElement.webkitRequestFullscreen !== undefined) this.domElement.webkitRequestFullscreen();
      else if (this.domElement.mozRequestFullScreen) this.domElement.mozRequestFullScreen();
      else if (this.domElement.msRequestFullscreen) this.domElement.msRequestFullscreen();
    } else {
      if (document.exitFullscreen) document.exitFullscreen();
      else if (document.cancelFullScreen) document.cancelFullScreen();
      else if (document.webkitCancelFullScreen) document.webkitCancelFullScreen();
      else if (document.mozCancelFullScreen) document.mozCancelFullScreen();
      else if (document.msExitFullscreen) document.msExitFullscreen();
    }

  },

  command_hide_all_icons: function() { DOW.log("command_hide_all_icons"); },

  command_quit: function() {
    DOW.log("command_quit");
    window.history.back();
  },

  
  command_show_grid: function() { DOW.log("command_show_grid"); },

  command_show_screen: function() {
    var btn = this.gui.buttons['show_screen'];
    if (this.screen1 !== undefined) {
      if (this.screen1.visible) {
        this.screen1.visible = false;
        if (btn) btn.src = btn.icon2;
      } else {
        this.screen1.visible = true;
        if (btn) btn.src = btn.icon1;
      }
    }
    DOW.log("command_show_screen");
  },

  command_reset_north: function() { DOW.log("command_reset_north"); },

  command_layer: function() { DOW.log("command_layer"); },

  command_forward_play: function() {
    DOW.log("command_forward_play");
    if (this.animation_status == 1) this.com_pause();
    else this.com_forward_play();
  },

  command_reverse_play: function() {
    DOW.log("command_reverse_play");
    if (this.animation_status == -1) this.com_pause();
    else this.com_reverse_play();
  },

  command_pause_or_play: function() {
    DOW.log("command_pause_or_play");
    if (this.animation_status != 0) this.com_pause();
    else this.com_forward_play();
  },

  com_forward_play: function() {
    this.animation_status = 1;
    this.animationController.speed = this.speedFromCKK(this.animation_speed) * this.animation_status;    
    this.animationController.play();
    // update GUI
    var btn = this.gui.buttons['forward_play'];
    if (btn) btn.src = btn.icon2;
    btn = this.gui.buttons['reverse_play'];
    if (btn) btn.src = btn.icon1;
  },

  com_reverse_play: function() {
    this.animation_status = -1;
    this.animationController.speed = this.speedFromCKK(this.animation_speed) * this.animation_status;
    this.animationController.play();
    // update GUI
    var btn = this.gui.buttons['reverse_play'];
    if (btn) btn.src = btn.icon2;
    btn = this.gui.buttons['forward_play'];
    if (btn) btn.src = btn.icon1;
  },

  com_pause: function() {
    this.animation_status = 0;
    this.animationController.pause();
    // update GUI
    var btn = this.gui.buttons['reverse_play'];
    if (btn) btn.src = btn.icon1;
    btn = this.gui.buttons['forward_play'];
    if (btn) btn.src = btn.icon1;
  },

  command_first_frame: function() {
    this.animationController.seek( 0.0 );
    DOW.log("command_first_frame");
  },

  command_last_frame: function() {
    this.animationController.seek( 1.0 );
    DOW.log("command_last_frame");
  },

  command_play_fast: function() {
    this.animation_speed ++;
    if (this.animation_speed > 32) this.animation_speed = 32;
    var spd = this.speedFromCKK(this.animation_speed) * this.animation_status;
    this.animationController.speed = spd;
    DOW.log("command_play_fast");
  },

  command_play_slow: function() {
    this.animation_speed --;
    if (this.animation_speed < 1) this.animation_speed = 1;
    var spd = this.speedFromCKK(this.animation_speed) * this.animation_status;
    this.animationController.speed = spd;
    DOW.log("command_play_slow");
  },

  speedFromCKK: function( ckk_val ) {
    return Math.pow( 2.0, ckk_val / 2.0 - 4.0 );
  },

  command_step_backward: function() {
    if (this.animation_status != 0) this.com_pause();
    this.animationController.step(-1);
    DOW.log("command_step_backward");
  },

  command_step_backward10: function() {
    if (this.animation_status != 0) this.com_pause();
    this.animationController.step(-10);
    DOW.log("command_step_backward10");
  },

  command_step_forward: function() {
    if (this.animation_status != 0) this.com_pause();
    this.animationController.step(1);
    DOW.log("command_step_forward");
  },

  command_step_forward10: function() {
    if (this.animation_status != 0) this.com_pause();
    this.animationController.step(10);
    DOW.log("command_step_forward10");
  },


  command_auto_spin: function() {
    var btn = this.gui.buttons['auto_spin'];
    if (this.spin_status) {
      this.spin_status = false;
      if (btn) btn.src = btn.icon1;
    } else {
      this.spin_status = true;
      if (btn) btn.src = btn.icon2;
    }
    DOW.log("command_auto_spin");
  },

  command_spin_fast: function() {
    this.spin_speed += 1;
    DOW.log("command_spin_fast");
  },

  command_spin_slow: function() {
    this.spin_speed -= 1;
    DOW.log("command_spin_slow");
  },


  command_zoom_in: function() {
    if (this.earth1) this.earth1.scale = this.earth1.scale * 1.1;
    this.trackball.radius = this.earth1.scale;
    DOW.log("command_zoom_in");
  },

  command_zoom_in_petit: function() {
    if (this.earth1) this.earth1.scale = this.earth1.scale * 1.01;
    this.trackball.radius = this.earth1.scale;
    DOW.log("command_zoom_in_petit");
  },

  command_zoom_out: function() {
    if (this.earth1) this.earth1.scale = this.earth1.scale / 1.1;
    this.trackball.radius = this.earth1.scale;
    DOW.log("command_zoom_out");
  },

  command_zoom_out_petit: function() {
    if (this.earth1) this.earth1.scale = this.earth1.scale / 1.01;
    this.trackball.radius = this.earth1.scale;
    DOW.log("command_zoom_out_petit");
  },


  command_screen_down: function() { DOW.log("command_screen_down"); },

  command_screen_large: function() { DOW.log("command_screen_large"); },

  command_screen_left: function() { DOW.log("command_screen_left"); },

  command_screen_right: function() { DOW.log("command_screen_right"); },

  command_screen_small: function() { DOW.log("command_screen_small"); },

  command_screen_up: function() { DOW.log("command_screen_up"); },

  // お絵かき機能（Web版は無効）
  command_show_drawing: function() { },
  command_drawing_pen: function() { },
  command_drawing_erase: function() { },
  command_drawing_undo: function() { },
  command_drawing_save: function() { },

  // レイヤーの切り替え
  command_change_layer: function() { DOW.log("command_change_layer"); },

  // 矢印キーの操作
  command_up_arrow_key: function() { },
  command_down_arrow_key: function() { },
  command_right_arrow_key: function() {
    if (this.config.PresentationRemoteMode || this.config.PresentaionRemoteMode) this.command_step_forward();
  },
  command_left_arrow_key: function() {
    if (this.config.PresentationRemoteMode || this.config.PresentaionRemoteMode) this.command_step_backward();
  },

  
  // 開発支援機能
  command_toggle_console: function() {
    this.console.toggle();
  },
  command_loading_console: function() {
    this.console.loading();
  },

};
