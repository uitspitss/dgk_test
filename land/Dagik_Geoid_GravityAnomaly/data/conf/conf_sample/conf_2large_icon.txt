!#TextureStart 24
#TextureEnd 96

#Spin 1

#ClickLock 1

#Animation 1 :[0: stop, 1: play, -1: reverse play]
#AnimationSpeed 8 :[-30,30]
#StopAt1stMap 2.
#StopAtFinalMap 1.

#ScreenFront 1
#ScreenScaleXY 3. -0.5 1.    : [Caption is center.]
!#ScreenScaleXY 6 -1.3 -1.    : Caption is in lower part.

#Icon 22   
#IconScaleXY 0. 2.4 -1.5		       ###Play/Stop
#Icon2ScaleXY 0. 1.9 -1.75	        ###Initial
#Icon3ScaleXY 0. 2.15 -1.5		     ###Reverse Play/Stop
#Icon4ScaleXY 0. 2.4 1.3		       ###Spin/Stop
#Icon5ScaleXY 0. 2.4 -1.75		      ###1step Forward
#Icon6ScaleXY 0. 2.15 -1.75		 ###1step Backward
#Icon7ScaleXY 0. 2.4 1.05		       ###Latitude/Longitude Lines
#Icon8ScaleXY 0. 2.4 1.55		       ###Zoom in
#Icon9ScaleXY 0. 2.15 1.55		      ###Zoom out
#Icon10ScaleXY 0. 2.15 1.3		      ###North upward
#Icon11ScaleXY 0. 2.4 -1.25	      ### Fast (Play speed)
#Icon12ScaleXY 0. 2.15 -1.25	      ### Slow (Play speed)
#Icon13ScaleXY 0. 2.4 1.95		        ###Quit
#Icon14ScaleXY 0. 2.15 1.95		  ###Full Screen
#Icon15ScaleXY 0. -2.20 1.95		  ###Logo Small
#Icon16ScaleXY 0. -1.995 1.95		  ###Logo Large
#Icon17ScaleXY 0. -2.65 1.85		  ###Logo Left
#Icon18ScaleXY 0. -2.45 1.85		  ###Logo Right
#Icon19ScaleXY 0. -2.55  1.65		  ###Logo Down
#Icon20ScaleXY 0. -2.55  2.05		  ###Logo Up
#Icon21ScaleXY 0. -0.85  -1.75		  ###Switch Layers
#Icon22ScaleXY 0. -1.25  -1.75		  ###Screen Show/Hide

#Icon21ScaleXY .9 -1.1   0.		  ###Switch Layers
#Icon22ScaleXY .9  0.2   0.		  ###Screen Show/Hide

#InertiaOfRotation 0.02 0.01 0.01 0.9999 : [Start_criteria, Stop_criteria, Speed of rotation, Damping rate (1= little damping, 0 = no inertia rotation)]
===================================================
